//#include "utilities.hpp"

#include "io/read_multilayer_network.hpp"
#include "io/write_multilayer_network.hpp"
#include "io/write_multilayer_communities.hpp"
#include "core/exceptions/WrongParameterException.hpp"
#include "community/abacus.hpp"
#include "community/glouvain2.hpp"
#include "community/infomap.hpp"
#include "community/louvain.hpp"
#include "community/mlp.hpp"
#include "community/ml-cpm.hpp"
#include "operations/flatten.hpp"
#include <iostream>
#include <fstream>


/**
* @brief maps back the communities found in the flattened network into multi-layer communities in the original multi-layer instance.
* @fComs: The communities of the flattened graph
* @mnet:  The input multi-layer network instance
* @return:The mapping as actor communities on the multi-layer instance
*/
std::unique_ptr<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>
map_back_to_ml(
               const uu::net::CommunityStructure<uu::net::Network>* fComs,
               const uu::net::MultilayerNetwork* mnet
)
{

    auto ml_communities = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();

   //for each community in the flattened network
   for (auto single_community: *fComs)
   {
       auto ml_community = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();

       //for each node in the current flattened network community
       for (auto actor: *single_community)
       {
           for (auto layer: *mnet->layers())
           {
               if (layer->vertices()->contains(actor))
               {
                   auto vertex = uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer);
                   ml_community->add(vertex);
               }
           }
       }
       // add this community to the list of communities to be returned
       ml_communities->add(std::move(ml_community));
   }

   return ml_communities;
}

std::unique_ptr<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>
map_back_to_ml(
               const uu::net::CommunityStructure<uu::net::WeightedNetwork>* fComs,
               const uu::net::MultilayerNetwork* mnet
)
{

    auto ml_communities = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();

   //for each community in the flattened network
   for (auto single_community: *fComs)
   {
       auto ml_community = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();

       //for each node in the current flattened network community
       for (auto actor: *single_community)
       {
           for (auto layer: *mnet->layers())
           {
               if (layer->vertices()->contains(actor))
               {
                   auto vertex = uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer);
                   ml_community->add(vertex);
               }
           }
       }
       // add this community to the list of communities to be returned
       ml_communities->add(std::move(ml_community));
   }

   return ml_communities;
}

int
main(
    int argc,
    char *argv[]
)
{
    typedef uu::net::MultilayerNetwork M;

    auto algorithm = std::string(argv[1]);
    auto input = std::string(argv[2]);
    auto output = std::string(argv[3]);
    
    auto net = uu::net::read_attributed_homogeneous_multilayer_network(input, "net", ',', true);
    
    std::unique_ptr<uu::net::CommunityStructure<M>> com;
    
    if (algorithm=="abacus_3_1")
    {
        size_t min_actors = 3;
        size_t min_layers = 1;
        com = uu::net::abacus<M>(net.get(), min_actors, min_layers);
    }
    else if (algorithm=="abacus_4_2")
    {
        size_t min_actors = 4;
        size_t min_layers = 2;
        com = uu::net::abacus<M>(net.get(), min_actors, min_layers);
    }
    else if (algorithm=="cpm_3_1")
    {
        size_t k = 3;
        size_t m = 1;
        com =  uu::net::mlcpm(net.get(), k, m);
    }
    else if (algorithm=="cpm_4_2")
    {
        size_t k = 4;
        size_t m = 2;
        com = uu::net::mlcpm(net.get(), k, m);
    }
    
    else if (algorithm=="flat_nw") {
        auto fnet = std::make_unique<uu::net::Network>("tmp", uu::net::EdgeDir::UNDIRECTED, true);
        uu::net::flatten_unweighted(net->layers()->begin(), net->layers()->end(), fnet.get());
        //result = label_propagation_single(fnet.get(), fLayer);
        auto single_communities = uu::net::louvain(fnet.get());
        com = map_back_to_ml(single_communities.get(), net.get());
    }
    else if (algorithm=="flat_ec") {
        auto fnet = std::make_unique<uu::net::WeightedNetwork>("tmp", uu::net::EdgeDir::UNDIRECTED, true);
        uu::net::flatten_weighted(net->layers()->begin(), net->layers()->end(), fnet.get());
        auto single_communities = uu::net::louvain(fnet.get());
        com = map_back_to_ml(single_communities.get(), net.get());
    }
    else if (algorithm=="glouvain_low") {
        double omega = .01;
        com = uu::net::glouvain2(net.get(), omega);
    }
    else if (algorithm=="glouvain_high") {
        double omega = 1;
        com = uu::net::glouvain2(net.get(), omega);
    }
    /*
    else if (algorithm=="lart") {
        uint32_t t = net->get_layers()->size()*3;//atoi(argv[3]);
        double eps = 1;//to_double(argv[4]);
        double gamma = 1;//to_double(argv[5]);
        lart algo;
        result = algo.fit(net.get(), t, eps, gamma);
    }
    else if (algorithm=="pmm") {
        int k = net->get_actors()->size()/10;//atoi(argv[3]);
        int ell = net->get_actors()->size()/10;//atoi(argv[4]);
        pmm algo;
        result = algo.fit(net.get(), k, ell);
    }
    */
    else if (algorithm=="mlp") {
        com = uu::net::mlp(net.get());
    }
    else if (algorithm=="infomap_n_o")
    {
        bool overlapping = false;
        com = uu::net::infomap(net.get(), overlapping);
    }
    else if (algorithm=="infomap_o")
    {
        bool overlapping = true;
        com = uu::net::infomap(net.get(), overlapping);
    }
    /*
    else if (algorithm=="ACLcut_c") {
        
        result = community_structure::create();
        int rw = 1; // 1 = classical, 0 = relaxed
        double ilw = 1.0;// default 1 for classical, 0.5 for relaxed --- check
        double tp = 0.15;
        acl aa(net.get(),rw,ilw,tp);


        double teleport = 0.85;
        double epsilon = 1.0;
        for (ActorSharedPtr a: *net->get_actors()) {
            vector<NodeSharedPtr> seeds;
            for (NodeSharedPtr i: *net->get_nodes(a)){
                seeds.push_back(i);
            }
            CommunitySharedPtr c =  aa.get_community(teleport,epsilon,seeds);
            result->add_community(c);
            std::cout << "actor --> " << a->name << " detected" << std:: endl;
        }
    }
    else if (algorithm=="ACLcut_r") {
        
        result = community_structure::create();
        
        int rw = 0; // 1 = classical, 0 = relaxed
        double ilw = 0.5;// default 1 for classical, 0.5 for relaxed --- check
        double tp = 0.15;
        acl aa(net.get(),rw,ilw,tp);


        double teleport = 0.85;
        double epsilon = 1.0;

        for (ActorSharedPtr a: *net->get_actors()) {
            vector<NodeSharedPtr> seeds;
            for (NodeSharedPtr i: *net->get_nodes(a)){
                    seeds.push_back(i);
            }
            CommunitySharedPtr c =  aa.get_community(teleport,epsilon,seeds);
            result->add_community(c);
            std::cout << "actor --> " << a->name << " detected" << std:: endl;
        }
    }
    */
    else
    {
        throw uu::core::WrongParameterException("unsupported algorithm: " + algorithm);
    }
    
    uu::net::write_multilayer_communities(com.get(), output);
    
    return 0;
}

