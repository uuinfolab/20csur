#include "io/read_multilayer_network.hpp"
#include "networks/MultilayerNetwork.hpp"
#include "community/louvain.hpp"
#include "measures/degree.hpp"
#include <fstream>

template <typename M>
void to_actor_list(
             M* net,
            const std::string& output
                         )
{
    std::ofstream outfile;
    outfile.open(output + "/actors.csv");
    
    size_t num_actors = net->actors()->size();
    for (size_t a=0; a<num_actors; a++)
    {
        auto actor = net->actors()->at(a);
        outfile << actor->name << std::endl;
        
    }
    outfile.close();
}

template <typename M>
void to_layer_list(
             M* net,
            const std::string& output
                         )
{
    std::ofstream outfile;
    outfile.open(output + "/layers.csv");
    
    size_t num_layers = net->layers()->size();
    for (size_t l=0; l<num_layers; l++)
    {
        auto layer = net->layers()->at(l);
        outfile << layer->name << std::endl;
        
    }
    outfile.close();
}

template <typename M>
void to_actor_indexes(
             M* net,
            const std::string& output
                         )
{
    std::ofstream outfile;
    outfile.open(output + "/actors_ids.txt");
    
    size_t num_actors = net->actors()->size();
    for (size_t a=0; a<num_actors; a++)
    {
        auto actor = net->actors()->at(a);
        outfile << actor->name << " " << (a+1) << std::endl;
        
    }
    outfile.close();
}

template <typename M>
void to_layer_indexes(
             M* net,
            const std::string& output
                         )
{
    std::ofstream outfile;
    outfile.open(output + "/layers_ids.txt");
    
    size_t num_layers = net->layers()->size();
    for (size_t l=0; l<num_layers; l++)
    {
        auto layer = net->layers()->at(l);
        outfile << layer->name << " " << (l+1) << std::endl;
        
    }
    outfile.close();
}

template <typename M>
void to_scml(
             M* net,
            const std::string& output
                         )
{
    std::ofstream outfile;
    outfile.open(output + "/scml.m");
    size_t num_actors = net->actors()->size();
    size_t num_layers = net->layers()->size();
    
    bool first = true;
    outfile << "data = zeros(" << (num_actors*num_actors*num_layers) << ", 1);" << std::endl;
    outfile << "A = reshape(data, " << num_actors << ", " << num_actors << ", " << num_layers << ");" << std::endl;
    for (size_t l=0; l<num_layers; l++)
    {
        auto layer = net->layers()->at(l);
        for (size_t a1=0; a1<num_actors; a1++)
        {
            // column
            auto actor1 = net->actors()->at(a1);
            for (size_t a2=0; a2<num_actors; a2++)
            {
                auto actor2 = net->actors()->at(a2);
                // row
                if (layer->edges()->get(actor1, actor2))
                {
                    outfile << "A(" << (a2+1) << "," << (a1+1) << "," << (l+1) << ") = 1;" << std::endl;
                }
            }
        }
    }
    outfile.close();
}

template <typename M>
void to_ecdm(
             M* net,
            const std::string& output
                         )
{
    for (size_t l_id=0; l_id<net->layers()->size(); l_id++)
    {
        auto layer = net->layers()->at(l_id);
        std::ofstream outfile;
        outfile.open(output + "/adj" + std::to_string(l_id) + ".txt");
        
        for (size_t a_id=0; a_id<net->actors()->size(); a_id++)
        {
            auto actor = net->actors()->at(a_id);
            if (!layer->vertices()->contains(actor))
            {
                outfile << (a_id+1) << std::endl;
                continue;
            }
            size_t deg = uu::net::degree(layer, actor);
            if (deg == 0)
            {
                outfile << (a_id+1) << std::endl;
                continue;
            }
            for (auto n: *layer->edges()->neighbors(actor))
            {
                outfile << (net->actors()->index_of(n)+1) << " ";
            }
            outfile << std::endl;
        }
        outfile.close();
    }
    
    for (size_t l_id=0; l_id<net->layers()->size(); l_id++)
    {
        auto layer = net->layers()->at(l_id);
        std::ofstream outfile;
        outfile.open(output + "/class" + std::to_string(l_id) + ".txt");
        
        auto comm = uu::net::louvain(layer);
        std::map<const uu::net::Vertex*, size_t> comm_id;
        size_t c_id = 1;
        for (auto c: *comm)
        {
            for (auto v: *c)
            {
                comm_id[v] = c_id;
            }
            c_id++;
        }
        
        for (auto actor: *net->actors())
        {
            size_t res = comm_id[actor];
            if (res == 0) outfile << (c_id++) << std::endl;
            else outfile << res << std::endl;
        }
        outfile.close();
    }
}

template <typename M>
void to_lart(
             M* net,
            const std::string& output
                         )
{
    std::ofstream outfile;
    outfile.open(output + "/net.ncol");
    size_t num_actors = net->actors()->size();
    size_t num_layers = net->layers()->size();
    
    for (size_t l=0; l<num_layers; l++)
    {
        auto layer = net->layers()->at(l);
        for (auto edge: *layer->edges())
        {
            size_t a1 = net->actors()->index_of(edge->v1);
            size_t a2 = net->actors()->index_of(edge->v2);
            outfile << (a1+1) << " " << (a2+1)<< " " << (l+1) << std::endl;
        }
    }
    outfile.close();
}

template <typename M>
void to_glouvain(
             M* net,
            const std::string& output
                         )
{
    size_t num_actors = net->actors()->size();
    size_t num_layers = net->layers()->size();
    
    for (size_t l=0; l<num_layers; l++)
    {
        std::ofstream outfile;
        outfile.open(output + "/net_layer" + std::to_string(l+1) + ".ncol");
        auto layer = net->layers()->at(l);
        for (auto edge: *layer->edges())
        {
            size_t a1 = net->actors()->index_of(edge->v1);
            size_t a2 = net->actors()->index_of(edge->v2);
            outfile << (a1+1) << " " << (a2+1) << std::endl;
        }
        outfile.close();
    }
}

template <typename M>
void to_mlink(
             M* net,
            const std::string& output
                         )
{
    std::ofstream outfile;
    outfile.open(output + "/edgelist.csv");
    size_t num_actors = net->actors()->size();
    size_t num_layers = net->layers()->size();
    
    for (size_t l=0; l<num_layers; l++)
    {
        auto layer = net->layers()->at(l);
        for (auto edge: *layer->edges())
        {
            size_t a1 = net->actors()->index_of(edge->v1);
            size_t a2 = net->actors()->index_of(edge->v2);
            outfile << (l+1) << "," << (a1+1) << "," << (a2+1) << std::endl;
        }
    }
    outfile.close();
}

int main( int argc, char *argv[] )
{

    std::string input = argv[1]; // network
    std::string output = argv[2]; // output file base name

    auto net = uu::net::read_attributed_homogeneous_multilayer_network(input, "net", ',');
    
    to_scml(net.get(), output);
    to_ecdm(net.get(), output);
    to_mlink(net.get(), output);
    to_lart(net.get(), output);
    to_glouvain(net.get(), output);
    to_actor_list(net.get(), output);
    to_layer_list(net.get(), output);
    to_actor_indexes(net.get(), output);
    to_layer_indexes(net.get(), output);

    return 0;
}

