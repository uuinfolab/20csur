//#include "utilities.hpp"

#include "io/read_multilayer_network.hpp"
#include "io/read_multilayer_communities.hpp"
#include "community/omega_index.hpp"




int
main(
    int argc,
    char *argv[]
)
{
    typedef uu::net::MultilayerNetwork M;

    auto network = std::string(argv[1]);
    auto community1 = std::string(argv[2]);
    auto community2 = std::string(argv[3]);
    
    auto net = uu::net::read_attributed_homogeneous_multilayer_network(network, "net", ',', true);
    
    size_t num_vertices = net->actors()->size() * net->layers()->size();
    
    auto com1 = uu::net::read_multilayer_communities(community1, net.get(), ',');
    auto com2 = uu::net::read_multilayer_communities(community2, net.get(), ',');
    
    if (com1->size() == 0 || com2->size() == 0)
    {
        std::cout << "nan" << std::endl;
        return 0;
    }
    
    double omega = uu::net::omega_index(com1.get(), com2.get(), num_vertices);
    std::cout << omega << std::endl;
    //std::cerr << com1->size() << " " << com2->size() << " " << omega << std::endl;
    
    return 0;
}

