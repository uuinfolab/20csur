#include "community/community_stats.hpp"
#include "io/read_multilayer_communities.hpp"
#include "io/read_multilayer_network.hpp"

#include <map>
#include <vector>

template <typename M>
std::vector<double>
get_stats(
                              const uu::net::CommunityStructure<M>* com,
          const M* net
          )
{
     
    std::vector<double> stats;
     
    // SIZE
    stats.push_back(com->size());
    
    // SIZE LARGEST COMMUNITY
    stats.push_back(uu::net::size_largest_community(com));

    // RATIO SIZE LARGEST COMMUNITY / SECOND LARGEST
    stats.push_back(uu::net::size_ratio_largest_communities(com));
    
    // % NODES IN AT LEAST ONE COMMUNITY
    stats.push_back(uu::net::ratio_vertices_in_communities(com, net));

    // % PILLARS
    stats.push_back(uu::net::ratio_actors_in_pillars(com, net));
    
    // % ACTOR OVERLAPPING
    stats.push_back(uu::net::ratio_actors_overlapping(com, net));
   
    // % NODE OVERLAPPING
    stats.push_back(uu::net::ratio_vertices_overlapping(com, net));
       
    // % Percentage of singlton communities %s
    stats.push_back(uu::net::ratio_singleton_communities(com));
    
    // MODULARITY
    //stats.push_back(modularity(net,result,1));

    //stats.push_back(extended_modularity(net,result,1));
    //std::cerr<< extended_modularity(net,result,1) << std::endl;
    // END OF STATS
    
    return stats; 
}


void
print_stats(
                 const std::vector<std::pair<double,double>>& avg_stats,
            bool sd
            )
{

  	std::cout.setf(std::ios::fixed, std::ios::floatfield);
    std::cout.precision(2);
    std::string sep = " & ";
    double stdev;
    // AVERAGE SIZE
    std::cout << floor(100*avg_stats.at(0).first)/100;
    stdev = floor(100*avg_stats.at(0).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    std::cout << sep;
    
    // SIZE LARGEST COMMUNITY
 	std::cout << floor(100*avg_stats.at(1).first)/100;
    stdev = floor(100*avg_stats.at(1).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    std::cout << sep;
	// RATIO SIZE LARGEST COMMUNITY / SECOND LARGEST
 	std::cout << floor(100*avg_stats.at(2).first)/100;
    stdev = floor(100*avg_stats.at(2).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    std::cout << sep;
	// %NODES IN AT LEAST ONE COMMUNITY
	std::cout << floor(100*avg_stats.at(3).first)/100;
    stdev = floor(100*avg_stats.at(3).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    std::cout << sep;
	// %PILLARS
 	std::cout << floor(100*avg_stats.at(4).first)/100;
    stdev = floor(100*avg_stats.at(4).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    std::cout << sep;
	// % ACTOR OVERLAPPING
 	std::cout << floor(100*avg_stats.at(5).first)/100;
    stdev = floor(100*avg_stats.at(5).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    std::cout << sep;
	// % NODE OVERLAPPING
 	std::cout << floor(100*avg_stats.at(6).first)/100;
    stdev = floor(100*avg_stats.at(6).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    std::cout << sep;	
	// % NUMBER OF SINGLETONS
 	std::cout << floor(100*avg_stats.at(7).first)/100;
    stdev = floor(100*avg_stats.at(7).second)/100;
    if(sd && stdev > 0) std::cout << " \\pm " << stdev;
    //std::cout << sep;	
	/* MODULARITY
 	std::cout << floor(100*avg_stats.at(8).first)/100;
    if(sd) std::cout << " \\pm " << floor(100*avg_stats.at(8).second)/100;
    std::cout << sep;
    
 	std::cout << floor(100*avg_stats.at(9).first)/100;
    if(sd) std::cout << " \\pm " << floor(100*avg_stats.at(9).second)/100;
    }
    else 
    {
    for(int i=0;i<avg_stats.size()-1; i++){
    	 std::cout << " - " << sep;
    	}
    	std::cout << " - ";
    }
     */
    std::cout << " \\\\ ";
    std::cout << std::endl;
}

std::vector<std::pair<double,double>>
mean_and_sd(
            const std::vector<std::vector<double>>& data,
            int iter
            )
{
    std::vector<std::pair<double,double>> res;
    
    
    for (size_t j = 0; j < data[0].size(); j++)
    {
    double sum = 0.0;
    double mean= 0.0;
    double sd= 0.0;
    double standardDeviation = 0.0;


    for (size_t i = 0; i < iter; i++)
    {
        sum += data[i][j];
    }

    mean = sum/data.size();

    for (int i = 0; i < iter; i++)
    {
        standardDeviation += std::pow(data[i][j] - mean, 2);
    }
        
        sd = std::sqrt(standardDeviation / iter);
        
        res.push_back(std::make_pair(mean, sd));
    }
    return res;
}


int main(
         int argc,
         char *argv[]
         )
{
    //std::cerr << "processing " << std::string(argv[2]) << " " << std::string(argv[1]) << std::endl;
    
    std::string algorithm = std::string(argv[1]);
    std::string network = std::string(argv[2]);
    std::string community = std::string(argv[3]);
    //std::string data_type = std::string(argv[3]); // real, synthetic
    size_t num_of_iterations = 1;
    bool has_multiple_iterations = false;
    if (argc == 5)
    {
        num_of_iterations = std::stoi(std::string(argv[4]));
        has_multiple_iterations = true;
    }
    
	std::vector<std::vector<double>> all_stats;
	std::vector<std::pair<double,double>> avg_stats;

	auto net = uu::net::read_attributed_homogeneous_multilayer_network(network, "net", ',', true);
	
   	for (size_t iteration=0; iteration<num_of_iterations; iteration++)
    {
    	//std::cerr << "reading clustering " << iteration << std::endl;
        std::string file_name;
        if (has_multiple_iterations)
        {
            file_name = community + "-" + std::to_string(iteration+1) + ".csv";
        }
        else
        {
            file_name = community;
        }
        auto com = uu::net::read_multilayer_communities(file_name, net.get(), ',');
        //std::cerr << "community read " << iteration << std::endl;
        all_stats.push_back(get_stats(com.get(), net.get()));
        
        /*for (int i=0; i<single_experiment_stats.size(); i++)
        {
            all_stats[i].push_back(single_experiment_stats.at(i));
        }*/
    }
    //std::cerr << "communities are read" << std::endl;
    avg_stats = mean_and_sd(all_stats, num_of_iterations);
    
    std::cout << algorithm << " & ";
    print_stats(avg_stats, true);
    //print_stats(avg_stats,false);
    return 0;
}
