/**
 * This file includes functions to generate different types of multilayer
 * community structures, and a main program to run these functions for
 * some selected parameters.
 */

#include <fstream>

#include "community/CommunityStructure.hpp"
#include "generation/sample.hpp"
#include "generation/standard_graphs.hpp"
#include "io/write_multilayer_communities.hpp"
#include "io/write_multilayer_network.hpp"
#include "networks/MultilayerNetwork.hpp"

/**
 * Assuming that actors have IDs from 0 to (num_of_actors-1),
 * and that actors in the same community have consecutive IDs,
 * this function creates a vector with the extreme actor IDs for
 * each community. E.g., {0, 10, 20}
 */
std::vector<size_t>
create_eq_seeds(
                size_t num_of_actors,
                size_t num_of_seeds
                )
{
    std::vector<size_t> seeds;
    seeds.push_back(0);
    for (size_t i=1; i<=num_of_seeds; i++)
    {
        seeds.push_back(seeds.at(i-1) + num_of_actors/num_of_seeds);
        //std::cout << seeds.at(i) << " " ;
    }
    //std::cout << std::endl;
    return seeds;
 } 


//these seeds will result in 10 communities of different size.
//the size of each community is given as a percentage of #actors in the perncentages vector
std::vector<size_t>
create_neq_seeds(
                 size_t num_of_actors,
                 std::vector<double> percentages
                 )
{

    std::vector<size_t> seeds;
    seeds.push_back(0);
    for (size_t i=1; i<=percentages.size(); i++)
    {
        seeds.push_back(seeds.at(i-1)+(percentages.at(i-1)*num_of_actors));
        //std::cout << seeds.at(i) << " " ;
    }
    //std::cout << std::endl;
    return seeds;
 }



void
create_pillar_eq_nover(
                       size_t num_of_layers,
                       size_t num_of_actors,
                       const std::string& synth_data_folder,
                       std::vector<double> p_internal,
                       std::vector<double> p_external
                       )
{

    // PILLAR EQ PART (i.e non overlapping communities + pillar communities + same size communities)
    auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
    
    // define communities
    auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
    std::vector<size_t> seeds = create_eq_seeds(num_of_actors, num_of_actors/10);
    for (size_t i=0; i<seeds.size()-1; i++)
    {
        auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
        for (size_t l=0; l<num_of_layers; l++)
        {
            auto layer = net->layers()->at(l);
            for (size_t a=seeds[i]; a<seeds[i+1] && a<net->actors()->size(); a++)
            {
                auto actor = net->actors()->at(a);
                c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
            }
        }
        com->add(std::move(c));
    }
    
    // sample edges
    uu::net::sample(net.get(), com.get(), p_internal, p_external);
    std::string base_name = synth_data_folder+"PEP-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
    uu::net::write_multilayer_network(net.get(), base_name+".mpx");
    uu::net::write_multilayer_communities(com.get(), base_name+".gt");
    std::ofstream outfile;
    outfile.open(base_name+".k");
    outfile << "K=" << com->size() << std::endl;
    outfile.close();
    
    outfile.open(base_name+".th");
    outfile << "0.1"  << std::endl;
    outfile.close();
}


void create_pillar_eq_over(
                           size_t num_of_layers,
                           size_t num_of_actors,
                           std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                           )
{

        // PILLAR EQ OVER (pillar overlapping same size communities)
        auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);

        // define communities
        auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
        std::vector<size_t> seeds = create_eq_seeds(num_of_actors, 10);
        for (size_t i=0; i<seeds.size()-1; i++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t l=0; l<num_of_layers; l++)
            {
                auto layer = net->layers()->at(l);
                for (size_t a=seeds[i]; a<seeds[i+1]+3 && a<net->actors()->size(); a++)
                {
                    auto actor = net->actors()->at(a);
                    c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                }
            }
            com->add(std::move(c));
        }
        
        // sample edges
        uu::net::sample(net.get(), com.get(), p_internal, p_external);
        std::string base_name = synth_data_folder+"PEO-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
        uu::net::write_multilayer_network(net.get(), base_name+".mpx");
        uu::net::write_multilayer_communities(com.get(), base_name+".gt");
        std::ofstream outfile;
        outfile.open(base_name+".k");
        outfile << "K=" << com->size() << std::endl;
        outfile.close();
        
        outfile.open(base_name+".th");
        outfile << "0.1"  << std::endl;
        outfile.close();
    
}


void create_spillar_eq_nover(
                             size_t num_of_layers,
                             size_t num_of_actors,
                             std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                             )
{


     // SEMI-PILLAR EQ PART (the communities aren't overlapping, they pillar coms are of same size)
    auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
    
        
        // define communities
        auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
        std::vector<size_t> seeds = create_eq_seeds(num_of_actors, 10);
        //create communities on the first n-1 layers
        for (size_t i=0; i<seeds.size()-1; i++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t l=0; l<num_of_layers-1; l++)
            {
                auto layer = net->layers()->at(l);
                for (size_t a=seeds[i]; a<seeds[i+1] && a<net->actors()->size(); a++)
                {
                    auto actor = net->actors()->at(a);
                    c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                }
            }
            com->add(std::move(c));
        }
        //create communities on the last layer
        auto layer = net->layers()->at(num_of_layers-1);
        for (size_t s=0; s<10; s++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t a=s; a<net->actors()->size(); a+=(num_of_actors/10))
            {
                auto actor = net->actors()->at(a);
                c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
            }
            com->add(std::move(c));
        }

        // sample edges
        uu::net::sample(net.get(), com.get(), p_internal, p_external);
        std::string base_name = synth_data_folder+"SEP-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
        uu::net::write_multilayer_network(net.get(), base_name+".mpx");
        uu::net::write_multilayer_communities(com.get(), base_name+".gt");
        std::ofstream outfile;
        outfile.open(base_name+".k");
        outfile << "K=" << com->size() << std::endl;
        outfile.close();
        
        outfile.open(base_name+".th");
        outfile << "0.1"  << std::endl;
        outfile.close();
}

void create_spillar_eq_over(
                            size_t num_of_layers,
                            size_t num_of_actors,
                            std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                            )
{
        
        // SEMI-PILLAR EQ OVER (the communities overlap)
  auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
        
        
        // define communities
            auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
            std::vector<size_t> seeds = create_eq_seeds(num_of_actors, 10);
            //create communities on the first n-1 layers
            for (size_t i=0; i<seeds.size()-1; i++)
            {
                auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
                for (size_t l=0; l<num_of_layers-1; l++)
                {
                    auto layer = net->layers()->at(l);
                    for (size_t a=seeds[i]; a<seeds[i+1]+3 && a<net->actors()->size(); a++)
                    {
                        auto actor = net->actors()->at(a);
                        c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                    }
                }
                com->add(std::move(c));
            }
            //create communities on the last layer (this will generate 9 coms)
            auto layer = net->layers()->at(num_of_layers-1);
            for (size_t s=0; s<10; s++)
            {
                auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
                for (size_t a=s; a<net->actors()->size(); a+=(num_of_actors/10))
                {
                    auto actor = net->actors()->at(a);
                    c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                }
                com->add(std::move(c));
            }
        
            // sample edges
            uu::net::sample(net.get(), com.get(), p_internal, p_external);
            std::string base_name = synth_data_folder+"SEO-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
            uu::net::write_multilayer_network(net.get(), base_name+".mpx");
            uu::net::write_multilayer_communities(com.get(), base_name+".gt");
            std::ofstream outfile;
            outfile.open(base_name+".k");
            outfile << "K=" << com->size() << std::endl;
            outfile.close();
            
            outfile.open(base_name+".th");
            outfile << "0.1"  << std::endl;
            outfile.close();
}


void create_spillar_neq_nover(
                             size_t num_of_layers,
                             size_t num_of_actors,
                             std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                             )
{


     // SEMI-PILLAR nEQ PART (the communities aren't overlapping, they pillar coms are of same size)
    auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
    
        
        // define communities
        auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
        std::vector<double> percentages = {0.3,0.2,0.15,0.1,0.05,0.05,0.05,0.05,0.03,0.02};
        std::vector<size_t> seeds = create_neq_seeds(num_of_actors, percentages);
        //create communities on the first n-1 layers
        for (size_t i=0; i<seeds.size()-1; i++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t l=0; l<num_of_layers-1; l++)
            {
                auto layer = net->layers()->at(l);
                for (size_t a=seeds[i]; a<seeds[i+1] && a<net->actors()->size(); a++)
                {
                    auto actor = net->actors()->at(a);
                    c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                }
            }
            com->add(std::move(c));
        }
        //create communities on the last layer
        auto layer = net->layers()->at(num_of_layers-1);
        for (size_t s=0; s<10; s++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t a=s; a<net->actors()->size(); a+=(num_of_actors/10))
            {
                auto actor = net->actors()->at(a);
                c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
            }
            com->add(std::move(c));
        }

        // sample edges
        uu::net::sample(net.get(), com.get(), p_internal, p_external);
        std::string base_name = synth_data_folder+"SNP-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
        uu::net::write_multilayer_network(net.get(), base_name+".mpx");
        uu::net::write_multilayer_communities(com.get(), base_name+".gt");
        std::ofstream outfile;
        outfile.open(base_name+".k");
        outfile << "K=" << com->size() << std::endl;
        outfile.close();
        
        outfile.open(base_name+".th");
        outfile << "0.1"  << std::endl;
        outfile.close();
}

void create_spillar_neq_over(
                            size_t num_of_layers,
                            size_t num_of_actors,
                            std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                            )
{
        
        // SEMI-PILLAR EQ OVER (the communities overlap)
  auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
        
        
        // define communities
            auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
            std::vector<double> percentages = {0.3,0.2,0.15,0.1,0.05,0.05,0.05,0.05,0.03,0.02};
            std::vector<size_t> seeds = create_neq_seeds(num_of_actors, percentages);
            //create communities on the first n-1 layers
            for (size_t i=0; i<seeds.size()-1; i++)
            {
                auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
                for (size_t l=0; l<num_of_layers-1; l++)
                {
                    auto layer = net->layers()->at(l);
                    for (size_t a=seeds[i]; a<seeds[i+1]+3 && a<net->actors()->size(); a++)
                    {
                        auto actor = net->actors()->at(a);
                        c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                    }
                }
                com->add(std::move(c));
            }
            //create communities on the last layer (this will generate 9 coms)
            auto layer = net->layers()->at(num_of_layers-1);
            for (size_t s=0; s<10; s++)
            {
                auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
                for (size_t a=s; a<net->actors()->size(); a+=(num_of_actors/10))
                {
                    auto actor = net->actors()->at(a);
                    c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                }
                com->add(std::move(c));
            }
        
            // sample edges
            uu::net::sample(net.get(), com.get(), p_internal, p_external);
            std::string base_name = synth_data_folder+"SNO-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
            uu::net::write_multilayer_network(net.get(), base_name+".mpx");
            uu::net::write_multilayer_communities(com.get(), base_name+".gt");
            std::ofstream outfile;
            outfile.open(base_name+".k");
            outfile << "K=" << com->size() << std::endl;
            outfile.close();
            
            outfile.open(base_name+".th");
            outfile << "0.1"  << std::endl;
            outfile.close();
}

void create_pillar_neq_nover(
                             size_t num_of_layers,
                             size_t num_of_actors,
                             std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                             )
{
    // PILLAR GRAD PART
    auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
    
        // define communities
        auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
        std::vector<double> percentages = {0.3,0.2,0.15,0.1,0.05,0.05,0.05,0.05,0.03,0.02};
        std::vector<size_t> seeds = create_neq_seeds(num_of_actors, percentages);
        for (size_t i=0; i<seeds.size()-1; i++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t l=0; l<num_of_layers; l++)
            {
                auto layer = net->layers()->at(l);
                for (size_t a=seeds[i]; a<seeds[i+1] && a<net->actors()->size(); a++)
                {
                    auto actor = net->actors()->at(a);
                    c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                }
            }
            com->add(std::move(c));
        }
        
        // sample edges
        uu::net::sample(net.get(), com.get(), p_internal, p_external);
        std::string base_name = synth_data_folder+"PNP-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
        uu::net::write_multilayer_network(net.get(), base_name+".mpx");
        uu::net::write_multilayer_communities(com.get(), base_name+".gt");
        std::ofstream outfile;
        outfile.open(base_name+".k");
        outfile << "K=" << com->size() << std::endl;
        outfile.close();
        
        outfile.open(base_name+".th");
        outfile << "0.1"  << std::endl;
        outfile.close();
}

void create_pillar_neq_over(
                            size_t num_of_layers,
                            size_t num_of_actors,
                            std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external)
{
    // PILLAR GRAD OVER
        auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
        
        auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
        std::vector<double> percentages = {0.3,0.2,0.15,0.1,0.05,0.05,0.05,0.05,0.03,0.02};
        std::vector<size_t> seeds = create_neq_seeds(num_of_actors, percentages);
        for (size_t i=0; i<seeds.size()-1; i++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t l=0; l<num_of_layers; l++)
            {
                auto layer = net->layers()->at(l);
                for (size_t a=seeds[i]; a<seeds[i+1]+3 && a<net->actors()->size(); a++)
                {
                    auto actor = net->actors()->at(a);
                    c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer));
                }
            }
            com->add(std::move(c));
        }
        
        
        uu::net::sample(net.get(), com.get(), p_internal, p_external);
        std::string base_name = synth_data_folder+"PNO-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
        uu::net::write_multilayer_network(net.get(), base_name+".mpx");
        uu::net::write_multilayer_communities(com.get(), base_name+".gt");
        std::ofstream outfile;
        outfile.open(base_name+".k");
        outfile << "K=" << com->size() << std::endl;
        outfile.close();
        
        outfile.open(base_name+".th");
        outfile << "0.1"  << std::endl;
        outfile.close();

}

void create_hierchical(
                               size_t num_of_layers,
                               size_t num_of_actors,
                               std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                               )
{
        // HIER PART
        auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
        
        auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
        
        std::vector<size_t> seeds1 = create_eq_seeds(num_of_actors, 10);
        std::vector<size_t> seeds2 = create_eq_seeds(num_of_actors, 5);
        std::vector<double> percentages = {0.4, 0.3, 0.3};
        std::vector<size_t> seeds3 = create_neq_seeds(num_of_actors,percentages);
        
        auto layer1 = net->layers()->at(0);
        for (size_t i=0; i<seeds1.size()-1; i++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t a=seeds1[i]; a<seeds1[i+1] && a<net->actors()->size(); a++)
            {
                auto actor = net->actors()->at(a);
                c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer1));
            }
            com->add(std::move(c));
        }
        
        auto layer2 = net->layers()->at(1);
        for (size_t i=0; i<seeds2.size()-1; i++)
        {
            auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
            for (size_t a=seeds2[i]; a<seeds2[i+1] && a<net->actors()->size(); a++)
            {
                auto actor = net->actors()->at(a);
                c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer2));
            }
            com->add(std::move(c));
        }
    
    auto layer3 = net->layers()->at(2);
    for (size_t i=0; i<seeds3.size()-1; i++)
    {
        auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
        for (size_t a=seeds3[i]; a<seeds3[i+1] && a<net->actors()->size(); a++)
        {
            auto actor = net->actors()->at(a);
            c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer3));
        }
        com->add(std::move(c));
    }
        
        
        uu::net::sample(net.get(), com.get(), p_internal, p_external);
        std::string base_name = synth_data_folder+"HIE-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
        uu::net::write_multilayer_network(net.get(), base_name+".mpx");
        uu::net::write_multilayer_communities(com.get(), base_name+".gt");
        std::ofstream outfile;
        outfile.open(base_name+".k");
        outfile << "K=" << com->size() << std::endl;
        outfile.close();
        
        outfile.open(base_name+".th");
        outfile << "0.1"  << std::endl;
        outfile.close();
}


void create_mix(
                         size_t num_of_layers,
                         size_t num_of_actors,
                         std::string synth_data_folder,
std::vector<double> p_internal,
std::vector<double> p_external
                         )
{

        // CORE GROUPS (overlapping + non overlapping single layer communities)
        auto net = uu::net::null_multiplex(num_of_actors, num_of_layers);
        auto layer1 = net->layers()->at(0);
        auto layer2 = net->layers()->at(1);
        auto layer3 = net->layers()->at(2);
        
        auto com = std::make_unique<uu::net::CommunityStructure<uu::net::MultilayerNetwork>>();
        
    {
    auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
              for (size_t a=0; a<10; a++)
              {
                  auto actor = net->actors()->at(a);
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer1));
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer2));
              }
              com->add(std::move(c));
    }
    
    
    {
    auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
              for (size_t a=40; a<50; a++)
              {
                  auto actor = net->actors()->at(a);
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer1));
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer2));
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer3));
              }
              com->add(std::move(c));
    }
    
    
    {
    auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
              for (size_t a=60; a<70; a++)
              {
                  auto actor = net->actors()->at(a);
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer2));
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer3));
              }
              com->add(std::move(c));
    }
    
    
    {
    auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
              for (size_t a=7; a<17; a++)
              {
                  auto actor = net->actors()->at(a);
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer1));
              }
              com->add(std::move(c));
    }
    
    
    {
    auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
              for (size_t a=15; a<30; a++)
              {
                  auto actor = net->actors()->at(a);
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer1));
              }
              com->add(std::move(c));
    }
    
    {
    auto c = std::make_unique<uu::net::Community<uu::net::MultilayerNetwork>>();
              for (size_t a=20; a<40; a++)
              {
                  auto actor = net->actors()->at(a);
                  c->add(uu::net::MLVertex<uu::net::MultilayerNetwork>(actor, layer3));
              }
              com->add(std::move(c));
    }
        
    
    uu::net::sample(net.get(), com.get(), p_internal, p_external);
    std::string base_name = synth_data_folder+"MIX-"+std::to_string(num_of_actors)+"-"+std::to_string(num_of_layers);
    uu::net::write_multilayer_network(net.get(), base_name+".mpx");
    uu::net::write_multilayer_communities(com.get(), base_name+".gt");
    std::ofstream outfile;
    outfile.open(base_name+".k");
    outfile << "K=" << com->size() << std::endl;
    outfile.close();
    
    outfile.open(base_name+".th");
    outfile << "0.1"  << std::endl;
    outfile.close();
}

int main( int argc, char *argv[] )
{
    
    auto synth_data_folder = std::string(argv[1]) + "/";
    
    int num_of_layers = 3;
    int num_of_actors = 100;
    std::vector<double> p_internal; // probabilities of edges within a communitiy
      std::vector<double> p_external; // probabilities of edges among communities
      for (size_t i=0; i<num_of_layers; i++)
      {
          p_internal.push_back(.5);
          p_external.push_back(.01);
      }
    
    //non-overlapping
    std::cout<<"create PEP..." <<std::flush;
    create_pillar_eq_nover(num_of_layers, num_of_actors, synth_data_folder, p_internal, p_external);
    for (size_t a=1000; a<=10000; a+=1000)
    {
        create_pillar_eq_nover(3, a, synth_data_folder, p_internal, p_external);
    }
    for (size_t l=1; l<=50; l++)
    {
        std::vector<double> p_internal;
          std::vector<double> p_external;
          for (size_t i=0; i<l; i++)
          {
              p_internal.push_back(.5);
              p_external.push_back(.01);
          }
        create_pillar_eq_nover(l, 100, synth_data_folder, p_internal, p_external);
    }
    std::cout<<"done!" <<std::endl;
	
     std::cout<<"create PEO..." <<std::flush;
    create_pillar_eq_over(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;

	
    std::cout<<"create PNP..." <<std::flush;
    create_pillar_neq_nover(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;

    std::cout<<"create PNO..." <<std::flush;
    create_pillar_neq_over(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;


    std::cout<<"create SEP..." <<std::flush;
    create_spillar_eq_nover(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;

    std::cout<<"create SEO..." <<std::flush;
    create_spillar_eq_over(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;
    
    
    std::cout<<"create SNP..." <<std::flush;
    create_spillar_neq_nover(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;

    std::cout<<"create SNO..." <<std::flush;
    create_spillar_neq_over(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;
    
    std::cout<<"create HIE..." <<std::flush;
    create_hierchical(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;
    
    std::cout<<"create MIX..." <<std::flush;
    create_mix(num_of_layers,num_of_actors,synth_data_folder, p_internal, p_external);
    std::cout<<"done!" <<std::endl;

    return 0;
}

