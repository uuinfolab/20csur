echo "Plotting pairwise comparisons..."

base_dir=$(pwd)

input_dir=$base_dir/output/results
echo "-input data directory set to: " $input_dir
output_dir=$base_dir/output/figures
echo "-input data directory set to: " $output_dir

source venv/bin/activate
python python/heatmap_pw_comp.py $input_dir/omega_pw.csv $output_dir omega #2> /dev/null
deactivate

echo "done!"
