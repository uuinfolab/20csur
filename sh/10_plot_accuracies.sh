echo "Plotting accuracy barplots..."

base_dir=$(pwd)

input_dir=$base_dir/output/results
echo "-input data directory set to: " $input_dir
output_dir=$base_dir/output/figures
echo "-input data directory set to: " $output_dir

source venv/bin/activate
python python/barplots.py $input_dir/omega_gt.csv $output_dir  2> /dev/null
deactivate

echo "done!"
