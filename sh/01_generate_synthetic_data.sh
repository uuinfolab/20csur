echo "Generating synthetic data..."

base_dir=$(pwd)

output_dir=$base_dir/input
echo "-output data directory set to: " $output_dir

# we call a C++ program
./build/generate_data.out $output_dir

echo "done!"
