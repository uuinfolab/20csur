echo "Printing ground truth summaries..."

base_dir=$(pwd)

net_dir=$base_dir/input
echo "-input network directory set to:" $net_dir
output_dir=$base_dir/output/tables
echo "-output data directory set to:" $output_dir


declare -a data1=("aucs" "dkpol" "PEP-100-3" "PNP-100-3" "PEO-100-3" "PNO-100-3" "SEP-100-3" "SNP-100-3" "SEO-100-3" "SNO-100-3" "HIE-100-3" "MIX-100-3")

for data in "${data1[@]}"
do
    echo "-processing dataset:" $data
    ./build/print_stats.out $data $net_dir/$data.mpx $net_dir/$data/gt.csv >> $output_dir/gt_summary.csv
done

echo "done!"
