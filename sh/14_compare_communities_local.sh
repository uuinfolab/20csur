echo "Processing local methods..."

base_dir=$(pwd)

input_dir=$base_dir/local_methods
echo "-input directory set to:" $input_dir
code_dir=$base_dir/python
echo "-input code directory set to:" $code_dir
output_dir=$base_dir/output/figures
echo "-output directory set to:" $output_dir


source venv/bin/activate

python $code_dir/heatmap_pw_comp_local.py $input_dir/comp_jaccard/real.csv $output_dir

python $code_dir/heatmap_pw_comp_local.py $input_dir/comp_jaccard/synthetic.csv $output_dir

python $code_dir/scalability_local.py $input_dir/scalability/actors.csv $output_dir na

python $code_dir/scalability_local.py $input_dir/scalability/layers.csv $output_dir nl

deactivate

echo "done!"
