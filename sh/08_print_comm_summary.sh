echo "Printing statistics of the generated communities..."

base_dir=$(pwd)

rep=10
echo "-algorithms repeated" $rep "times"
net_dir=$base_dir/input
echo "-input data directory set to:" $net_dir
comm_dir=$base_dir/output/communities
echo "-output data directory set to:" $comm_dir
output_dir=$base_dir/"output/tables"
echo "-output data directory set to:" $output_dir

declare -a datasets=("aucs" "dkpol" "airports" "rattus"
                "PEP-100-3" "PNP-100-3" "PEO-100-3" "PNO-100-3" "SEP-100-3" "SNP-100-3" "SEO-100-3" "SNO-100-3" "HIE-100-3" "MIX-100-3")

declare -a algo=("flat_nw" "flat_ec" "abacus_3_1" "abacus_4_2" "emcd" "pmm_l" "pmm_h" "scml" "cpm_3_1" "cpm_4_2" "lart" "infomap_n_o" "infomap_o" "glouvain_low" "glouvain_high" "mlp")

for data in "${datasets[@]}"
do
    for algorithm in "${algo[@]}"
    do
        ./build/print_stats.out $algorithm $net_dir/$data.mpx $comm_dir/comm-$algorithm-$data $rep >> $output_dir/comm_summary_$data.csv
    done
done

echo "done"
