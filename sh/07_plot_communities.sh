echo "Plotting communities..."

base_dir=$(pwd)

net_dir=$base_dir/input
echo "-input data directory set to:" $net_dir
com_dir=$base_dir/output/communities
echo "-input comm directory set to:" $com_dir
output_dir=$base_dir/output/communities_viz
echo "-output data directory set to:" $output_dir

declare -a datasets=("toy" "aucs" )

declare -a algo=("flat_nw" "flat_ec" "abacus_3_1" "abacus_4_2" "cpm_3_1" "cpm_4_2" "glouvain_low" "glouvain_high" "infomap_n_o" "infomap_o" "scml" "pmm_l" "pmm_h" "lart" "emcd" "mlp")


for algorithm in "${algo[@]}"
do
    echo "-plotting communities for:" $algorithm
    for data in "${datasets[@]}"
    do
        Rscript R/plot.R $net_dir/$data.mpx $com_dir/comm-$algorithm-$data-1.csv $output_dir/comm-$algorithm-$data.png > /dev/null 2>&1
    done
done

echo "done!"
