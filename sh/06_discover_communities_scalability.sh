echo "Generating communities for scalability test..."

base_dir=$(pwd)

timeout=3600
echo "-timeout set to:" $timeout
#declare -a repeat=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10")
declare -a repeat=("1" "2" "3" "4" "5")
echo "-repeating each algorithm" ${#repeat[@]} "times"
max_layers=50
echo "-max number of layers to be tested:" $max_layers
algorithms_dir=$base_dir/algorithms
chmod +x algorithms/*
echo "-directory with algorithms set to:" $algorithms_dir
input_dir=$base_dir/input
echo "-input data directory set to:" $input_dir
output_dir=$base_dir/output/results
result_file_layers=$output_dir/"scalability_layers.csv"
echo "-output file for layer scalability set to:" $result_file_layers

declare -a algo=("flat_nw" "flat_ec" "abacus_3_1" "abacus_4_2" "emcd" "pmm_l" "pmm_h" "scml" "cpm_3_1" "cpm_4_2" "lart" "infomap_n_o" "infomap_o" "glouvain_low" "glouvain_high" "glouvain_scal"  "mlink" "mlp")
echo algorithm,iteration,n_actors,n_layers,noise,exec_time > $result_file_layers

for algorithm in "${algo[@]}"
do
	for lay in $(seq 1 1 $max_layers)
	do
        echo "-executing: " $algorithm.sh on PEP-100-$lay
		for rep in "${repeat[@]}"
		do
            t1=$(($(gdate +%s%N)/1000000))
            gtimeout $timeout $algorithms_dir/$algorithm.sh $input_dir/PEP-100-$lay tmp.csv
            if [ $? -eq 124 ]
            then
                echo $algorithm,$rep,100,$lay,0,"$timeout"000 >> $result_file_layers
                continue 3
            fi
            t2=$(($(gdate +%s%N)/1000000))
            echo $algorithm,$rep,100,$lay,0,$(expr $t2 - $t1) >> $result_file_layers
		done
	done
done


result_file_actors=$output_dir/scalability_actors.csv
echo "-output file for actor scalability set to:" $result_file_actors

declare -a actors=("100" "1000" "2000" "3000" "4000" "5000" "6000" "7000" "8000" "9000" "10000")


echo algorithm,iteration,n_actors,n_layers,noise,exec_time > $result_file_actors

for algorithm in "${algo[@]}"
do
    for act in "${actors[@]}"
    do
        echo "executing: " $algorithm.sh on PEP-$act-3
        for rep in "${repeat[@]}"
        do
            t1=$(($(gdate +%s%N)/1000000))
            gtimeout $timeout $algorithms_dir/$algorithm.sh $input_dir/PEP-$act-3 tmp.csv
            if [ $? -eq 124 ]
            then
                echo $algorithm,$rep,$act,3,0,"$timeout"000 >> $result_file_actors
                continue 3
            fi
            t2=$(($(gdate +%s%N)/1000000))
            echo $algorithm,$rep,$act,3,0,$(expr $t2 - $t1) >> $result_file_actors
        done
    done
done

rm tmp.csv

echo "done!"
