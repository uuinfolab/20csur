echo "Computing accuracy for data with ground truth..."

base_dir=$(pwd)

repetitions=10
echo "-algorithms repeated" $repetitions "times"
net_dir=$base_dir/input
echo "-network data directory set to:" $net_dir
comm_dir=$base_dir/output/communities
echo "-community data directory set to:" $comm_dir
output_dir=$base_dir/output/results
echo "-output directory set to:" $output_dir

declare -a datasets=("toy" "aucs" "dkpol" "PEP-100-3" "PNP-100-3" "PEO-100-3" "PNO-100-3" "SEP-100-3" "SNP-100-3" "SEO-100-3" "SNO-100-3" "HIE-100-3" "MIX-100-3")

declare -a algo=("flat_nw" "flat_ec" "abacus_3_1" "abacus_4_2" "emcd" "pmm_l" "pmm_h" "scml" "cpm_3_1" "cpm_4_2" "lart" "infomap_n_o" "infomap_o" "glouvain_low" "glouvain_high" "mlp")

echo dataset,algorithm,rep,value_type,value > $output_dir/omega_gt.csv

for data in "${datasets[@]}"
do
    echo "-processing dataset:" $data
	for algorithm in "${algo[@]}"
	do
        for rep in $(seq 1 1 $repetitions)
        do
            if test -f $comm_dir/comm-$algorithm-$data-$rep.csv; then
                echo -n $data,$algorithm,$rep,F, >> $output_dir/omega_gt.csv
                ./build/compare.out $net_dir/$data.mpx $comm_dir/comm-$algorithm-$data-$rep.csv $net_dir/$data/gt.csv >> $output_dir/omega_gt.csv
            fi
        done
	done
done

echo "done!"
