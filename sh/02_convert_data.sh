echo "Converting data to different input formats..."

base_dir=$(pwd)

input_dir=$base_dir/input
echo "-input data directory set to:" $input_dir
max_layers=50
echo "-max number of layers:" $max_layers

# data to test the produced communities

declare -a data=("aucs" "dkpol" "airports" "rattus" "toy" "PEP-100-3" "PNP-100-3" "PEO-100-3" "PNO-100-3" "SEP-100-3" "SNP-100-3" "SEO-100-3" "SNO-100-3" "HIE-100-3" "MIX-100-3")

for dat in "${data[@]}"
do
    echo -converting $dat
    mkdir $input_dir/$dat
    mv $input_dir/$dat.k $input_dir/$dat/k.m
    mv $input_dir/$dat.th $input_dir/$dat/theta.txt
    mv $input_dir/$dat.gt $input_dir/$dat/gt.csv
    ./build/convert_data.out $input_dir/$dat.mpx $input_dir/$dat
done

# data for scalability tests: varying the number of layers

for lay in $(seq 1 1 $max_layers)
do
    echo -converting PEP-100-$lay
    mkdir $input_dir/PEP-100-$lay
    mv $input_dir/PEP-100-$lay.k $input_dir/PEP-100-$lay/k.m 2> /dev/null
    mv $input_dir/PEP-100-$lay.th $input_dir/PEP-100-$lay/theta.txt 2> /dev/null
    mv $input_dir/PEP-100-$lay.gt $input_dir/PEP-100-$lay/gt.csv 2> /dev/null
    ./build/convert_data.out $input_dir/PEP-100-$lay.mpx $input_dir/PEP-100-$lay
done

# data for scalability tests: varying the number of actors

declare -a actors=("100" "1000" "2000" "3000" "4000" "5000" "6000" "7000" "8000" "9000" "10000")

for act in "${actors[@]}"
do
    echo -converting PEP-$act-3
    mkdir $input_dir/PEP-$act-3
    mv $input_dir/PEP-$act-3.k $input_dir/PEP-$act-3/k.m 2> /dev/null
    mv $input_dir/PEP-$act-3.th $input_dir/PEP-$act-3/theta.txt 2> /dev/null
    mv $input_dir/PEP-$act-3.gt $input_dir/PEP-$act-3/gt.csv 2> /dev/null
    ./build/convert_data.out $input_dir/PEP-$act-3.mpx $input_dir/PEP-$act-3
done

echo "done!"
