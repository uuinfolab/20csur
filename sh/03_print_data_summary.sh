echo "Printing data summaries..."

base_dir=$(pwd)

# real data

output_dir=$base_dir/output/tables
echo "-output data directory set to:" $output_dir

declare -a data1=("aucs" "dkpol" "airports" "rattus")

for data in "${data1[@]}"
do
    echo "-processing dataset:" $data
    Rscript R/summary.R $data >> $output_dir/data_summary_real.csv 2> /dev/null
done

# synthetic data

declare -a data2=("PEP-100-3" "PNP-100-3" "PEO-100-3" "PNO-100-3" "SEP-100-3" "SNP-100-3" "SEO-100-3" "SNO-100-3" "HIE-100-3" "MIX-100-3")

for data in "${data2[@]}"
do
    echo "-processing dataset:" $data
    Rscript R/summary.R $data >> $output_dir/data_summary_synt.csv 2> /dev/null
done

echo "done!"
