echo "Plotting scalability (global)..."

base_dir=$(pwd)

input_dir=$base_dir/output/results
echo "-input data directory set to:" $input_dir
code_dir=$base_dir/python
echo "-input code directory set to:" $code_dir
output_dir=$base_dir/output/figures
echo "-input data directory set to:" $output_dir


source venv/bin/activate

python $code_dir/scalability_plots.py $input_dir/scalability_layers.csv $output_dir nl global #> /dev/null

python $code_dir/scalability_plots.py $input_dir/scalability_actors.csv $output_dir na global #> /dev/null

deactivate

echo "done!"
