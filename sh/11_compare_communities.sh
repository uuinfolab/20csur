echo "Pairwise comparison of found communities..."

base_dir=$(pwd)

net_dir=$base_dir/input
echo "-network data directory set to:" $net_dir
comm_dir=$base_dir/output/communities
echo "-community data directory set to:" $comm_dir
output_dir=$base_dir/output/results
echo "-output directory set to:" $output_dir

declare -a datasets=("toy" "aucs" "dkpol" "airports" "rattus" "PEP-100-3" "PNP-100-3" "PEO-100-3" "PNO-100-3" "SEP-100-3" "SNP-100-3" "SEO-100-3" "SNO-100-3" "HIE-100-3" "MIX-100-3")

declare -a algo=("flat_nw" "flat_ec" "abacus_3_1" "abacus_4_2" "emcd" "pmm_l" "pmm_h" "scml" "cpm_3_1" "cpm_4_2" "lart" "infomap_n_o" "infomap_o" "glouvain_low" "glouvain_high" "mlp")


echo dataset,algorithm1,algorithm2,value > $output_dir/omega_pw.csv

for data in "${datasets[@]}"
do
    echo "-processing dataset:" $data
	for algorithm1 in "${algo[@]}"
	do
        for algorithm2 in "${algo[@]}"
		do
            if [[ $algorithm1 < $algorithm2 ]]
            then continue
            fi
            if test -f $comm_dir/comm-$algorithm1-$data-1.csv; then
                if test -f $comm_dir/comm-$algorithm2-$data-1.csv; then
                    echo -n $data,$algorithm1,$algorithm2, >> $output_dir/omega_pw.csv
                    ./build/compare.out $net_dir/$data.mpx $comm_dir/comm-$algorithm1-$data-1.csv $comm_dir/comm-$algorithm2-$data-1.csv >> $output_dir/omega_pw.csv
                fi
            fi
            
		done
	done
done

echo "done!"
