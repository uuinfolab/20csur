echo "Generating communities for community analysis..."

base_dir=$(pwd)

timeout=3600
echo "-timeout set to:" $timeout
declare -a repeat=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10")
echo "-repeating each algorithm" ${#repeat[@]} "times"
algorithms_dir=$base_dir/algorithms
chmod +x $algorithms_dir/*
echo "-directory with algorithms set to:" $algorithms_dir
input_dir=$base_dir/input
echo "-input data directory set to:" $input_dir
output_dir=$base_dir/output/communities
echo "-output data directory set to:" $output_dir

declare -a datasets=("aucs" "toy" "dkpol" "airports" "rattus"
"PEP-100-3" "PNP-100-3" "PEO-100-3" "PNO-100-3" "SEP-100-3" "SNP-100-3" "SEO-100-3" "SNO-100-3" "HIE-100-3" "MIX-100-3")

declare -a algo=("flat_nw" "flat_ec" "abacus_3_1" "abacus_4_2" "emcd" "pmm_l" "pmm_h" "scml" "cpm_3_1" "cpm_4_2" "lart" "infomap_n_o" "infomap_o" "glouvain_low" "glouvain_high" "mlp")

# "mlink" not used for pairwise comparisons: it returns a different type of communities

for algorithm in "${algo[@]}"
do
	for data in "${datasets[@]}"
	do
        echo "-executing: " $algorithm.sh on $data
        #if [ $data == "dkpol" ] && [ $algorithm == "cpm_3_1" ]
        then
            echo TIMEOUT
            continue
        fi
		for rep in "${repeat[@]}"
		do
            gtimeout $timeout $algorithms_dir/$algorithm.sh $input_dir/$data $output_dir/comm-$algorithm-$data-$rep.csv
            if [ $? -eq 124 ]
            then
               echo TIMEOUT
               break
            fi
		done
	done
done

echo "done!"
