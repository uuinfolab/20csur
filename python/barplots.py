import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np 
import seaborn as sns
import sys
from matplotlib.colors import ListedColormap



input_data_path = sys.argv[1]
output_data_path = sys.argv[2]



#the input dataset should be four columns (dataset, algorithm, value_type, value)

data = pd.read_csv(input_data_path)
data = data.replace(-1.1, 0)
#overlapping_methods = ['Infomap(o)','ML-CPM(31)', 'ML-CPM(42)','ABACUS(31)', 'ABACUS(42)']
#partitioning_methods = ['Infomap(no)','GLouvain(hOm)','GLouvain(lOm)', 'MLP','WF_EC', 'NWF','LART']

#data= data.loc[data['algorithm'].isin(partitioning_methods)]
#data= data.loc[data['algorithm'].isin(overlapping_methods)]

datasets = data['dataset'].unique()

sns.set(font_scale=2.5)
#we have to gnerate a barplot for each (dataset,measure)
for dataset in datasets:
    #print(dataset + " ," + measure)  
    partial = pd.DataFrame(data[data.columns[1:]][(data["dataset"] == dataset)])
    #print(partial)
    if not partial.empty:
        #sns.set_style("whitegrid")
        #palette= sns.color_palette("coolwarm", n_colors=7)
        g = sns.factorplot(x="algorithm", y="value", legend=False, data=partial,size=10, kind="bar")
        g.despine(left=True)
        g.set_ylabels('Accuracy')
        g.set_xlabels('Algorithm')
        g.axes[0,0].set_ylim(-0.1,1.1)
        g.set_xticklabels(rotation=80)
        plt.legend(loc=2,ncol=2)
        plt.savefig(output_data_path+'/'+ dataset + '_accuracy.eps',format='eps', dpi=1000,bbox_inches='tight')
        plt.close()
