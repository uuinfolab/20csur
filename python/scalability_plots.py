import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np 
import seaborn as sns
import sys
from matplotlib.colors import ListedColormap


#sns.set(font_scale=2.5)


input_data_path = sys.argv[1]
output_data_path = sys.argv[2]
with_respect_to = sys.argv[3] # na for "number of actors", nl for "number of layers", n for "noise"
track = sys.argv[4] # "local" or "global"



#The input dataset should be seven columns (algorithm,iteration,n_actors,n_layers,noise,exec_time,accuracy)

data = pd.read_csv(input_data_path)

#average the execution time in all the iterations for each combination (algorithm,#actors,#layers,#noise)

iter_aggregation = {
    'exec_time': 'median'
}
data = data.groupby(['algorithm','n_actors','n_layers','noise']).agg(iter_aggregation)
#print(data)


data = data.add_suffix('_Count').reset_index()
data.columns=['algorithm','n_actors','n_layers','noise','exec_time']
data['exec_time'] = data['exec_time']/1000 #from milliseconds to minutes

#print(data)


algorithms = data['algorithm'].unique()

#print(algorithms)

# Initialize the figure
plt.style.use('seaborn-darkgrid')

# create a color palette
palette = plt.get_cmap('tab20b')
    
# multiple line plot

#print(algo)
#get the sub dataframe from the original one
#data_slice = data[data['algorithm']==algo]
#print(data_slice)
variable_name = ""
x_max= 1;
y_max=60
x_axes_title="";
if (with_respect_to=="na"):
    x_max= 10001
    variable_name="n_actors"
    x_axes_title = "Number of Actors"
else:
    if (with_respect_to=="nl"):
        x_max=21
        variable_name="n_layers"
        x_axes_title= "Number of Layers"

style = ['solid', 'dotted', 'dashed', 'dashdot', 'solid', 'dotted', 'dashed', 'dashdot','solid']
mark = ['.', 'o', '^', 'v', 's', 'x', '<', '>','d']

if (variable_name=="n_actors"):
    for i in range(len(algorithms)):
        algo = algorithms[i]
        data_slice = data[data['algorithm']==algo]
        plt.plot(variable_name, 'exec_time', data=data_slice, color='red', linestyle=style[i%8], linewidth=1.5, marker=mark[i%8], markersize=8, alpha=0.4, label=algo)
        if (i+1)%8 == 0:
            plt.xlim(0,x_max)
            plt.xlabel(x_axes_title)
            plt.ylabel('Execution time (Seconds)')
            plt.ylim(0,y_max)
            plt.legend(loc=0,ncol=1)
            plt.savefig(output_data_path + '/' + variable_name+'_'+str(i)+'.pdf', bbox_inches='tight')
            plt.close()
else:
    for i in range(len(algorithms)):
        algo = algorithms[i]
        data_slice = data[data['algorithm']==algo]
        plt.plot(variable_name, 'exec_time', data=data_slice, color='red', linestyle=style[i%8], linewidth=1.5, marker=mark[i%8], markersize=8, alpha=0.4, label=algo)
        if (i+1)%8 == 0:
            plt.xlim(0,x_max)
            plt.xlabel(x_axes_title)
            plt.ylabel('Execution time (Seconds)')
            plt.ylim(0,y_max)
            plt.legend(loc=0,ncol=1)
            plt.savefig(output_data_path + '/' + variable_name+'_'+str(i)+'.pdf', bbox_inches='tight')
            plt.close()

if (i+1)%8 != 0:
    plt.xlim(0,x_max)
    plt.xlabel(x_axes_title)
    plt.ylabel('Execution time (Seconds)')
    plt.ylim(0,y_max)
    plt.legend(loc=0,ncol=1)
    plt.savefig(output_data_path + '/' + variable_name+'_'+track+'_'+str(i)+'.pdf', bbox_inches='tight')
    plt.close()

