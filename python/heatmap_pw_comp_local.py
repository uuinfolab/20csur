import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np 
import seaborn as sns
import sys
import matplotlib as mpl
from matplotlib.colors import ListedColormap


input_data_path = sys.argv[1]
output_data_dir = sys.argv[2]

 

#the input dataset should be four columns (dataset, algorithm1, algortihm2, jaccard)

data = pd.read_csv(input_data_path)
#print(data)

datasets = data['dataset'].unique()
print(datasets)
sns.set(font_scale=2)

for dataset in datasets:
    partial = pd.DataFrame(data.loc[data['dataset'] == dataset])
    partial = partial.iloc[:, 1:] #choose the last three columns
    iter_aggregation = {
        'jaccard_index': ['mean','std']
        }

    partial = partial.groupby(['algorithm1','algorithm2']).agg(iter_aggregation)
    #print(partial)
    partial = partial.add_suffix('_Count').reset_index()
    partial.columns= ['algorithm1','algorithm2','mean','std']

    partial_mean = partial.pivot('algorithm1','algorithm2','mean')
    partial_mean= partial_mean.fillna(1)
    partial_std = partial.pivot('algorithm1','algorithm2','std')
    partial_std= partial_std.fillna(0)

    print(partial_mean)
    print(partial_std)
    
    plt.clf()
    cmap = ListedColormap(sns.color_palette("Spectral", 14))
    h = sns.heatmap(partial_mean,cmap=cmap,vmin=0,vmax=1,annot=True,annot_kws={"size": 18}, center=0.5, square=True, linewidths=.7, cbar_kws={"shrink": .99})
    h.set_xlabel('')
    h.set_ylabel('')
    h.set_xticklabels(h.get_xticklabels(),rotation=75)
    h.set_yticklabels(h.get_yticklabels(),rotation='horizontal')
    

    plt.savefig(output_data_dir+'/'+ dataset + '_hm_jacc_mean.eps',format='eps', dpi=1000,bbox_inches='tight')
    plt.close()

    plt.clf()
    cmap = ListedColormap(sns.color_palette("Reds"),7)
    h = sns.heatmap(partial_std,cmap=cmap,vmin=0,vmax=1, annot=True,center=0.5, square=True, linewidths=.7, cbar_kws={"shrink": .99})
    h.set_xlabel('')
    h.set_ylabel('')
    h.set_xticklabels(h.get_xticklabels(),rotation=75)
    h.set_yticklabels(h.get_yticklabels(),rotation='horizontal')
    

    plt.savefig(output_data_dir+'/'+ dataset + '_hm_jacc_std.eps',format='eps', dpi=1000,bbox_inches='tight')
    plt.close()
    
