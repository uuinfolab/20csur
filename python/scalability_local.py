import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np 
import seaborn as sns
import sys
from matplotlib.colors import ListedColormap


sns.set(font_scale=2.5)


input_data_path = sys.argv[1]
output_data_dir = sys.argv[2]
with_respect_to = sys.argv[3] # na for "number of actors", nl for "number of layers", n for "noise"



#The input dataset should be four columns (algorithm,seed,n_actors,n_layers,noise,exec_time)

data = pd.read_csv(input_data_path)


if(with_respect_to=="nl"):
    data['n_actors']=1000

#average the execution time in all the iterations for each combination (algorithm,#actors,#layers,#noise)

iter_aggregation = {
    'exec_time': ['median','std']
    }

data = data.groupby(['algorithm','n_actors','n_layers','noise']).agg(iter_aggregation)


data = data.add_suffix('_Count').reset_index()
data.columns= ['algorithm','n_actors','n_layers','noise','median','std']

#print(data)

data['median'] = data['median']/1000 #from milliseconds to minutes
data['std'] = data['std']/1000 #from milliseconds to minutes
#data['mean'] = data['mean'].apply(lambda x: x if x <= 60 else 60)#print(data)

algorithms = data['algorithm'].unique()

#print(algorithms)

# Initialize the figure
plt.style.use('seaborn-darkgrid')
 
# create a color palette
palette = plt.get_cmap('tab20b')
 
# multiple line plot
for algo in algorithms:

    #print(algo)
    #get the sub dataframe from the original one
    data_slice = data[data['algorithm']==algo]
    #print(data_slice)
    variable_name = ""
    x_max= 1;
    x_axes_title="";
    if(with_respect_to=="na"):
        x_max= 10500
        variable_name="n_actors"
        x_axes_title = "Number of Actors"
    else:
        if(with_respect_to=="nl"):
            x_max=55
            variable_name="n_layers"
            x_axes_title= "Number of Layers"


    if (variable_name=="n_actors"):
        data = data[data['n_layers']==2]
        #in this case we will plot 3 lines in each sub_plot (one for each #layers)
        data_slice_2 = data_slice[data_slice['n_layers']==2]
        plt.plot(list(data_slice_2['n_actors']), list(data_slice_2['median']), color='red', linewidth=1.5,marker='^',markersize=8,alpha=0.4)
        

        #plt.legend(loc=0,ncol=1,fontsize=15)
    else :
        std_plus = np.add(list(data_slice['median']),list(data_slice['std']))
        #std_plus[std_plus>65]=65
        #std_minus = np.subtract(list(data_slice['mean']),list(data_slice['std']))
        #print(std_minus)
        #std_minus[std_minus<-2]=-2
        #print(std_minus)
        plt.plot(variable_name, 'median', data=data_slice, color='green',marker='o',markersize=5,alpha=0.7, linewidth=2)
        #plt.plot(list(data_slice['n_layers']), std_plus, color='gray', linewidth=1.5,marker='^',markersize=8,alpha=0.4)
        #plt.plot(list(data_slice['n_layers']), std_minus, color='gray', linewidth=1.5,marker='^',markersize=8,alpha=0.4, label="l=2")



    plt.xlim(0,x_max)
    plt.xlabel(x_axes_title)
    plt.ylabel('Execution time (Seconds)')
    if with_respect_to== "na":
        plt.ylim(-1,50)
        #print("hi")
    else:
        plt.ylim(-1,50)
    #plt.show()
    plt.savefig(output_data_dir+'/'+ variable_name+ "_" + algo + ".pdf",bbox_inches='tight')
    plt.close()

