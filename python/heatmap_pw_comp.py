import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np 
import seaborn as sns
import sys
import matplotlib as mpl

input_data_path = sys.argv[1]
output_data_path = sys.argv[2]
measure_type = sys.argv[3]

from matplotlib.colors import ListedColormap

#the input dataset should be four columns (dataset, algorithm1, algorithm2, value)

data = pd.read_csv(input_data_path)
#print(data)
data.drop_duplicates(subset=['dataset','algorithm1','algorithm2'], keep='first', inplace=True)
#print(data)
datasets = data['dataset'].unique()

#sns.set(font_scale=1)

for dataset in datasets:
    partial = pd.DataFrame(data.loc[data['dataset'] == dataset])
    partial = partial.iloc[:, 1:] #choose the last three columns
    partial.columns = ['key1','key2','value']
    partial = partial.pivot('key1','key2','value')
    #print(partial)
    cmap=""
    h=""
    if(measure_type=="nmi"):
        cmap = ListedColormap(sns.color_palette("Reds"),7)
        h = sns.heatmap(partial,cmap=cmap,mask=(partial==-1.1),vmin=0,vmax=1, center=0.5, square=True, linewidths=.7, cbar_kws={"shrink": .3}, annot=True, annot_kws={"size": 7}, fmt='.2f')
    else:
        cmap = ListedColormap(sns.color_palette("Spectral", 14))
        h = sns.heatmap(partial, cmap=cmap, mask=(partial==-1.1), vmin=-1, vmax=1, center=0.3, square=True, linewidths=.7, cbar_kws={"shrink": .8}, annot=True, annot_kws={"size": 7}, fmt='.2f', xticklabels = 1, yticklabels = 1)
    
    
    #cmap1 = mpl.colors.ListedColormap(['w'])
    #h = sns.heatmap(partial,cmap=cmap1,mask=(partial!=-1.1), linewidths=.7, cbar=False)
    h.set_xlabel('')
    h.set_ylabel('')
    h.set_xticklabels(h.get_xticklabels(), rotation=75, fontsize = 10)
    h.set_yticklabels(h.get_yticklabels(), rotation='horizontal', fontsize = 10)
            
    plt.savefig(output_data_path + '/' + dataset + '_hm_'+ measure_type +'.eps',format='eps', dpi=1000,bbox_inches='tight')
    plt.close()
