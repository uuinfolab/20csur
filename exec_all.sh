#########################
# COMPILE C++ PROGRAMS  #
#########################

mkdir build
cd build
cmake ..
make
cd ..

#########################
# PREPARE THE WORKSPACE #
#########################

# make the programs and scripts executable

chmod +x ./build/*.out
chmod +x ./sh/*.sh
chmod +x ./algorithms/*.sh

# create the directories for the experiments

mkdir input # all input data will be put here
mkdir output
mkdir output/communities # communities produced by the tested methods
mkdir output/communities_viz # plots of communities for simple datasets (basic check)
mkdir output/tables # output latex tables included in the article
mkdir output/figures # output figures (plots, etc.) included in the article
mkdir output/results # output files with raw data used to produce tables and figures

base_dir=$(pwd)

########################################
# PREPARE THE DATA FOR THE EXPERIMENTS #
########################################

# copy the real datasets to the input directory

cp data/* input

# generate synthetic datasets for accuracy and scalability analysis

./sh/01_generate_synthetic_data.sh

# convert all datasets to the formats used by the different algorithms

./sh/02_convert_data.sh

# print statistics about the data (including ground truth community structures)

./sh/03_print_data_summary.sh
./sh/04_print_gt_summary.sh

#######################
# RUN ALGORITHMS      #
#######################

# run global algorithms on datasets to analyze the resulting communities

./sh/05_discover_communities_accuracy.sh

# run scalability tests varying number of actors and layers

./sh/06_discover_communities_scalability.sh

# this produces plots of the communities for two small networks, to visually check the results
# it requires the multinet library installed on R
# ./sh/07_plot_communities.sh

# print statistics about the resulting communities

./sh/08_print_comm_summary.sh

# compare the communities produced by different algorithms with the ground truth, where present

./sh/09_compare_gt.sh

./sh/10_plot_accuracies.sh

# compare the communities produced by different algorithms, pairwise

./sh/11_compare_communities.sh

./sh/12_plot_comparisons.sh

# scalability plots

./sh/13_plot_scalability.sh

# plot results of local methods

./sh/14_compare_communities_local.sh
