normadj.m: compute normalized adjacency matrix
rnorm.m: normalize each row of a matrix
sfkernel.m: compute the spectral kernel
specluster.m: spectral clustering algorithm of Shi et al. (2000)
specluster2.m: spectral clustering algorithm of Ng et al. (2001)
specluster3.m: modified version of spectral clustering algorithm of Ng et al. (2001)
eval_sc.m: evaluate clustering performance based on spectral clustering
eval_kmeans.m: evaluate one-shot clustering performance based on k-means
eval_kmeans_repeat.m: evaluate average clustering performance based on k-means
purity.m: compute purity
NMI.m: compute normalized mutual information
RI.m: compute rand index
confumat.m: compute the confusion matrix
purity_idx.m: compute purity (vector input)
NMI_idx.m: compute normalized mutual information  (vector input)
RI_idx.m: compute rand index  (vector input)
confumat_idx.m: compute the confusion matrix (vector input)
entro.m: compute the entropy
groupcmp.m: get the common objects shared by two discrete sets