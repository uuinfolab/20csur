source venv/bin/activate

cd algorithms/GenLouvain
num_a=$(cat $1/actors.csv | wc -l)
num_l=$(cat $1/layers.csv | wc -l)

echo "read_mnet_and_exec('$1', $num_l, $num_a, 'com.txt')" > main.m
octave main.m
#python emcd_coms_convertor.py  $1/$theta/consensus.txt $1/actors_ids.txt $2

cd ..
cd ..

deactivate
