source venv/bin/activate

cd algorithms/EMCD
num_a=$(cat $1/actors.csv | wc -l)
num_l=$(cat $1/layers.csv | wc -l)
theta=$(cat $1/theta.txt)

mkdir $1/$theta > /dev/null 2>&1
java -jar -Xmx16G generateGreedy.jar $num_l $num_a $theta $1 # > /dev/null 2>&1
python emcd_coms_convertor.py  $1/$theta/consensus.txt $1/actors_ids.txt $2

cd ..
cd ..

deactivate
