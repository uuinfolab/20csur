#The following packages need to be included and up to date with the latest version of python:
'numpy','scipy','igraph','networkx', 'matplotlib','scipy' and 'sklearn'. Make sure to use the latest official version of 'sklearn' to obtain the correct results from the AgglomerativeClustering function. This is the python-sklearn (0.16.1-2~nd14.04+1) package obtained from NeuroDebian. 

#Running an example: Once you have installed all necessary packages on command line in the folder where LART.py is saved run
	python LART.py
For extra examples you can also run
	python LART.py 'simple' 

or any other of the simulation scenarios. There are five types of simulation scenarios provided: 'simple' corresponds to S1 in the paper; 'unifying' and 'unifying2' correspond to scenario S2 in paper; 'divisive' which corresponds to S3 in paper, and 'complex' corresponds to S4 in paper. The codes for the mixed scenario S5 are to be provided soon.

#Please ignore the connectivity warning produced, this has been taken care of in the code by using function 'updateDt' which makes sure that the completing of the connectivity matrix does not affect the final results.

#There are five types of simulation scenarios provided: 'simple' corresponds to S1 in the paper; 'unifying' and 'unifying2' correspond to scenario S2 in paper; 'divisive' which corresponds to S3 in paper, and 'complex' corresponds to S4 in paper. The codes for the mixed scenario S5 are to be provided soon.

#The main function is LART(a,eps,t,gamma,linkage,n_jobs1,network)
 - 'a' are the layers of the multiplex in form of a 3 dimensional numpy array
 - eps is the epsilon constant addedd to guarantee for completeness and non-bipartiteness. This can be between 0 and 1. I have assigned 1 since we work with undirected binary networks and this corresponds to adding a self loop to each node in each layer (similar to Walktrap algorithm). This could be varied. Please, do not assigne value higher than 1 since this will mask the topology of the original layers. 
 - t is the number of time steps - further simulations have indicated that values between 7-12 provide fairly similar results, but I have fixed 9 here
 - gamma is the resolution parameter again fixed to 1  - further simulations show that LART is fairly robust to this value and any other value between 0.5 and 1.5 will provide almost identical results
 - linkage - 'average' or 'complete'; in the paper we use 'average' although depending on the nature of the communities 'complete' might provide better results
 - n_jobs1 can be changed to any number of cores that the user wants to involve in the computation process
 - network - 'full' or 'dynamic'; The main functions are adapted for a dynamic network case as well, although this still needs to be tested and the visualization results need to be adapted accordingly

#The outputs of LART are Dt,part,mod,maxmod=LART(a,eps,t,gamma,linkage,n_jobs1,network)
 - Dt - node pairwise dissimilarity matrix
 - part - the node cluster assignment of length LxN (L layers and N nodes in each layer). part[0:N] - node assignement in first layer, part[N:2N] - node assignment in second layers, etc. 
 - mod - all multiplex modularity values for the full hierarchical structure
 - maxmod - max multiplex modularity value obtained based on the hierarchical structure

NB for very large networks the functions numpy.linalg.matrix_power(P,t) to get the P^t transition matrix and Dmat(Pt,L,D,n_jobs1=n_jobs1) to get the pairwise distance matrix can be further parallelized to save time. This will be embedded in the code shortly. 
	
For any questions related to the code please contact z.kuncheva12@imperial.ac.uk. 

