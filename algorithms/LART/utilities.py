import numpy as np
import igraph as ig
import operator
import networkx as nx

def getAmatrix(input_path):
    layer_graphs = {}
    layer_edges = {}
    layer_vertices = {}
    f = open(input_path,'r')
    for line in f:
        vals = line.split(" ")
        layer = int(vals[2].strip())
        if layer not in layer_graphs:
            layer_graphs[layer] = ig.Graph()
            layer_edges[layer] = []
            layer_vertices[layer] = set()
    f.close()
    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        layer = int(vals[2].strip())
        a = vals[0].strip()
        b = vals[1].strip()
        for ly in layer_graphs:
            if a not in layer_vertices[ly]:
                layer_vertices[ly].add(a)
            if b not in layer_vertices[ly]:
                layer_vertices[ly].add(b)
        layer_edges[layer].append((a,b))
    for l in layer_graphs:
        layer_graphs[l].add_vertices(len(layer_vertices[l]))
        layer_graphs[l].vs['name'] = list(layer_vertices[l])
        layer_graphs[l].add_edges(layer_edges[l])
    L = len(layer_graphs)
    N = len(layer_vertices[1])
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.array(layer_graphs[l].get_adjacency().data)
        Mat[i, :, :] = mat
    return Mat

def getAmatrix_mpx(input_path):
    layer_graphs = {}
    layer_edges = {}
    layer_vertices = {}

    start_read = True
    f = open(input_path, 'r')
    for line in f:
        if line.strip() == "#EDGES":
            start_read = False
    f.close()

    f = open(input_path, 'r')
    for line in f:
        if start_read:
            vals = line.split(",")
            layer = vals[2].strip()
            if layer not in layer_graphs:
                layer_graphs[layer] = ig.Graph()
                layer_edges[layer] = []
                layer_vertices[layer] = set()
        if line.strip() == "#EDGES":
            start_read = True
    f.close()

    start_read = True
    f = open(input_path, 'r')
    for line in f:
        if line.strip() == "#EDGES":
            start_read = False
    f.close()

    f = open(input_path, 'r')
    for line in f:
        if start_read:
            vals = line.split(",")
            layer = vals[2].strip()
            a = vals[0].strip()
            b = vals[1].strip()
            for ly in layer_graphs:
                if a not in layer_vertices[ly]:
                    layer_vertices[ly].add(a)
                if b not in layer_vertices[ly]:
                    layer_vertices[ly].add(b)
            layer_edges[layer].append((a,b))
        if line.strip() == "#EDGES":
            start_read = True
    lab = ""
    for l in layer_graphs:
        lab = l
        layer_graphs[l].add_vertices(len(layer_vertices[l]))
        layer_graphs[l].vs['name'] = list(layer_vertices[l])
        layer_graphs[l].add_edges(layer_edges[l])
    L = len(layer_graphs)
    N = len(layer_vertices[lab])
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.array(layer_graphs[l].get_adjacency().data)
        Mat[i, :, :] = mat
    return Mat


# uses list instead of set, matrix is deterministic (for comparison with c++)
def getAmatrix2(input_path):
    layer_graphs = {}
    layer_edges = {}
    layer_vertices = {}
    f = open(input_path,'r')
    for line in f:
        vals = line.split(" ")
        layer = int(vals[2].strip())
        if layer not in layer_graphs:
            layer_graphs[layer] = ig.Graph()
            layer_edges[layer] = []
            layer_vertices[layer] = []
    f.close()
    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        layer = int(vals[2].strip())
        a = vals[0].strip()
        b = vals[1].strip()
        for ly in layer_graphs:
            if a not in layer_vertices[ly]:
                layer_vertices[ly].append(a)
            if b not in layer_vertices[ly]:
                layer_vertices[ly].append(b)
        layer_edges[layer].append((a,b))
    for l in layer_graphs:
        layer_graphs[l].add_vertices(len(layer_vertices[l]))
        layer_graphs[l].vs['name'] = list(layer_vertices[l])
        layer_graphs[l].add_edges(layer_edges[l])
    L = len(layer_graphs)
    N = len(layer_vertices[1])
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.array(layer_graphs[l].get_adjacency().data)
        Mat[i, :, :] = mat
    return Mat


def getAmatrixCompactInput(input_path):
    layer_graphs = {}
    layer_edges = {}
    layer_vertices = {}
    f = open(input_path,'r')
    for line in f:
        vals = line.split(" ")
        layers = vals[2].strip().split(',')
        for layer in layers:
            if layer not in layer_graphs:
                layer_graphs[layer] = ig.Graph()
                layer_edges[layer] = []
                layer_vertices[layer] = set()
    f.close()
    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        layers = vals[2].strip().split(',')
        a = int(vals[0].strip())
        b = int(vals[1].strip())
        for ly in layer_graphs:
            if a not in layer_vertices[ly]:
                layer_vertices[ly].add(a)
            if b not in layer_vertices[ly]:
                layer_vertices[ly].add(b)
        for layer in layers:
            layer_edges[layer].append((a,b))
    for l in layer_graphs:
        layer_graphs[l].add_vertices(len(layer_vertices[l]))
        layer_graphs[l].vs['name'] = list(layer_vertices[l])
        layer_graphs[l].add_edges(layer_edges[l])
    L = len(layer_graphs)
    N = len(layer_vertices[0])
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.array(layer_graphs[l].get_adjacency().data)
        Mat[i, :, :] = mat
    return Mat

def partitionsProcessing(part,a):
    part_dict = {}
    assign_dict = {}
    N = len(a[0])
    L = len(a)
    start = 0
    end = 1
    while end <= L:
        #print part[start * N:end * N]
        c = 0
        for u in part[start * N:end * N]:
            if start not in part_dict:
                part_dict[start] = {}
            if u not in part_dict[start]:
                part_dict[start][u] = []
            part_dict[start][u].append(c)
            if c not in assign_dict:
                assign_dict[c]={}
            old = assign_dict[c].get(u,0)
            assign_dict[c][u]=old+1
            c += 1
        start += 1
        end += 1
    f = open("./coms.txt", 'w')
    for l in part_dict:
        for x in part_dict[l]:
            for a in part_dict[l][x]:
                f.write(str(a+1) + "," + str(l+1) + ","+ str(x) + "\n")
    f.close()

    merged_parts = {}
    #for c in assign_dict:
    #    com =max(assign_dict[c].items(), key=operator.itemgetter(1))[0]
    #    if com not in merged_parts:
    #        merged_parts[com] = []
    #    merged_parts[com].append(c)
    #f = open("output/majority_partition.txt",'w')
    #for x in merged_parts:
    #    f.write(str(x) + " : " + str(merged_parts[x]) + "\n")
    #f.close()
    return merged_parts

def readGraph(input_path):
    layer_graphs = {}
    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        layer = int(vals[2].strip())
        if layer not in layer_graphs:
            layer_graphs[layer] = nx.Graph()
        layer_graphs[layer].add_edge(int(vals[0].strip()), int(vals[1].strip()))
    return layer_graphs



"""
# toglie nodi di padding
def partitionsProcessing_wFilter(part,a,layer_graphs):
    part_dict = {}
    assign_dict = {}
    N = len(a[0])
    L = len(a)
    start = 0
    end = 1
    while end <= L:
        #print part[start * N:end * N]
        c = 0
        for u in part[start * N:end * N]:
            if start not in part_dict:
                part_dict[start] = {}
            if u not in part_dict[start]:
                part_dict[start][u] = []
            part_dict[start][u].append(c)
            if c not in assign_dict:
                assign_dict[c]={}
            old = assign_dict[c].get(u,0)
            assign_dict[c][u]=old+1
            c += 1
        start += 1
        end += 1

    for l in part_dict:
        f = open(str(l) + "part.txt", 'w')
        for x in part_dict[l]:
            f.write(str(x) + " : " + str(part_dict[l][x]) + "\n")
        f.close()

    merged_parts = {}
    for c in assign_dict:
        com =max(assign_dict[c].iteritems(), key=operator.itemgetter(1))[0]
        if com not in merged_parts:
            merged_parts[com] = []
        merged_parts[com].append(c)
    f = open("majority_partition.txt",'w')
    for x in merged_parts:
        f.write(str(x) + " : " + str(merged_parts[x]) + "\n")
    f.close()
    return merged_parts
"""

if __name__ == '__main__':
    f = "/home/interdonato/Dropbox/DATA/Danish Politics ML dataset/DK_pol.mpx"
    a = getAmatrix_mpx(f)
    print(a)
