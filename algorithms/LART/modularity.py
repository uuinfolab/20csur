

def multislice_modularity(layer_graphs,communities):
    gamma = 1.0 #teoricamente se ne puo' definire uno per layer
    w = 1.0 #interlayer weight: 1 per i coupling, 0 altrimenti [CHECK]
    assignments = {}
    c = 0
    for D_key in communities:
        D = communities[D_key]
        for n in D:
            assignments[n]=c
        c+=1
    mu = 0.0 #total edge weight
    Q = 0.0
    for l in layer_graphs:
        for i in layer_graphs[l].nodes():
            sum_i = 0.0
            ni = layer_graphs[l].neighbors(i)
            deg_i = len(ni)
            for j in layer_graphs[l].neighbors(i):
                if i in assignments and j in assignments:
                    if assignments[i]==assignments[j]: #teoricamente gli assegn. possono essere diversi per ogni layer
                        deg_j = len(layer_graphs[l].neighbors(j))
                        P_ij = float(deg_i-deg_j)/float(2*len(layer_graphs[l].edges()))
                        sum_i+= 1.0-gamma*P_ij
            sum_i+=w  #contributo dell'arco coupling: nel nostro caso il nodo e' univoco, quindi direi 1?
            Q+=sum_i
            mu+=deg_i+(len(layer_graphs.keys())-1)*w    # grado su ogni layer + perso coupling edges (uno per layer)
    Q=float(Q)/float(2*mu)
    return Q
