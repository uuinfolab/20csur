#! /usr/bin/python

import numpy as np
import sys
import scipy.linalg as LA
import numpy.linalg
import igraph as ig
from scipy.sparse import coo_matrix
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import pairwise_distances
from utilities import getAmatrix
from utilities import getAmatrix2
from utilities import getAmatrix_mpx
from utilities import partitionsProcessing
from modularity import multislice_modularity
from utilities import readGraph
import os

def supraA(a, eps, method):
    A = LA.block_diag(*a)
    L = np.shape(a)[0]
    N = np.shape(a)[1]
    np.fill_diagonal(A, eps)
    if method == 'full':
        for i in np.arange(L - 1):
            for j in np.arange((i + 1), L):
                d = np.multiply(a[i, :, :], a[j, :, :]).sum(axis=0)
                np.fill_diagonal(A[(i * N):((i + 1) * N):1, (j * N):((j + 1) * N):1], d + eps)
                np.fill_diagonal(A[(j * N):((j + 1) * N):1, (i * N):((i + 1) * N):1], d + eps)
    if method == 'dynamic':
        for i in np.arange(L - 1):
            j = i + 1
            d = np.multiply(a[i, :, :], a[j, :, :]).sum(axis=0)
            np.fill_diagonal(A[(i * N):((i + 1) * N):1, (j * N):((j + 1) * N):1], d + eps)
            np.fill_diagonal(A[(j * N):((j + 1) * N):1, (i * N):((i + 1) * N):1], d + eps)
    return A


def diagA(a):
    d = a.sum(axis=0)
    d = 1 / d
    d[d == np.inf] = 0
    D = np.diag(d)
    return D


def Dmat(Pt, L, D, n_jobs1):
    N = int(np.shape(Pt)[0] / L)
    newP = np.dot(Pt, np.sqrt(D))
    Dmat = np.zeros(shape=[N * L, N * L])
    for i in np.arange(L):
        Dmat[(i * N):((i + 1) * N):1, (i * N):((i + 1) * N):1] = pairwise_distances(newP[(i * N):((i + 1) * N):1, ::],
                                                                                    n_jobs=n_jobs1)
    for i in np.arange(L - 1):
        for j in np.arange((i + 1), L):
            if i != j:
                newx = newP[(i * N):((i + 1) * N):1, (i * N):((i + 1) * N):1]
                newy = newP[(j * N):((j + 1) * N):1, (j * N):((j + 1) * N):1]
                newx = np.hstack((newx, newP[(i * N):((i + 1) * N):1, (j * N):((j + 1) * N):1]))
                newy = np.hstack((newy, newP[(j * N):((j + 1) * N):1, (i * N):((i + 1) * N):1]))
                l = np.arange(L)
                l = np.setdiff1d(l, np.array([i, j]))
                if len(l) > 0:
                    for k in np.arange(len(l)):
                        newx = np.hstack((newx, newP[(i * N):((i + 1) * N):1, (l[k] * N):((l[k] + 1) * N):1]))
                        newy = np.hstack((newy, newP[(j * N):((j + 1) * N):1, (l[k] * N):((l[k] + 1) * N):1]))
                dmat = pairwise_distances(newx, newy, n_jobs=n_jobs1)
                Dmat[(i * N):((i + 1) * N):1, (j * N):((j + 1) * N):1] = dmat
                Dmat[(j * N):((j + 1) * N):1, (i * N):((i + 1) * N):1] = np.transpose(dmat)
    return Dmat


def modmat(a, gamma, network):
    M = supraA(a, 0, network)
    twoum = M.sum()
    L = np.shape(a)[0]
    N = np.shape(a)[1]
    for i in np.arange(L):
        d = a[i, :, :].sum(axis=1)
        M[(i * N):((i + 1) * N):1, (i * N):((i + 1) * N):1] = M[(i * N):((i + 1) * N):1,
                                                              (i * N):((i + 1) * N):1] - gamma * (np.outer(d, d)) / (
        a[i, :, :].sum())
    M = M / twoum
    return M


class cluster_node:
    def __init__(self, id=None, left=None, right=None, orig=None):
        self.left = left
        self.right = right
        self.orig = orig
        self.id = id


def getorder(x):
    clust = [cluster_node(id=i, orig=i) for i in range(np.shape(x)[0] + 1)]
    for i in np.arange(len(x)):
        if clust[x[i, 0]].left is None and clust[x[i, 1]].left is None:
            newclust = cluster_node(id=len(clust), orig=x[i,], left=x[i, 0], right=x[i, 1])
            clust.append(newclust)
        if clust[x[i, 0]].left is None and clust[x[i, 1]].left is not None:
            newclust = cluster_node(id=len(clust), orig=np.concatenate(([x[i, 0]], clust[x[i, 1]].orig)), left=x[i, 0],
                                    right=x[i, 1])
            clust.append(newclust)
        if clust[x[i, 0]].left is not None and clust[x[i, 1]].left is None:
            newclust = cluster_node(id=len(clust), orig=np.concatenate((clust[x[i, 0]].orig, [x[i, 1]])), left=x[i, 0],
                                    right=x[i, 1])
            clust.append(newclust)
        if clust[x[i, 0]].left is not None and clust[x[i, 1]].left is not None:
            newclust = cluster_node(id=len(clust), orig=np.concatenate((clust[x[i, 0]].orig, clust[x[i, 1]].orig)),
                                    left=x[i, 0], right=x[i, 1])
            clust.append(newclust)
    return clust


def modMLPX(x, a, gamma, network):
    L = np.shape(a)[0]
    N = np.shape(a)[1]
    lr = np.arange(L).repeat(N)
    lbl = np.arange(L * N)
    # tot=a.sum()
    # deg=a.sum(axis=1).flatten()
    # div=a.sum(axis=(1,2)).repeat(N)
    M = modmat(a, gamma, network)
    mod = [np.diag(M).sum()]
    for i in np.arange((N * L), len(x)):
        data = x[i]
        v1 = x[data.left].orig
        if type(v1) == int:
            v1 = [v1]
        v2 = x[data.right].orig
        if type(v2) == int:
            v2 = [v2]
        s1 = 2 * (M[np.ix_(v1, v2)].sum())
        s2 = mod[-1] + s1
        mod.append(s2)
    mod = np.array(mod)
    return mod


class cluster_part:
    def __init__(self, vals=None):
        self.vals = vals


def fm_index(x, y=None):
    """ Return the Fowlkes-Mallows index. """
    cont = x if y is None else contingency_table(x, y, norm=False)
    a, b, c, d = rand_values(cont)
    return a / (np.sqrt((a + b) * (a + c)))


def rand_values(cont_table):
    """Calculate values for rand indices."""
    n = cont_table.sum()
    sum1 = (cont_table * cont_table).sum()
    sum2 = (cont_table.sum(axis=1) ** 2).sum()
    sum3 = (cont_table.sum(axis=0) ** 2).sum()
    a = (sum1 - n) / 2.0;
    b = (sum2 - sum1) / 2
    c = (sum3 - sum1) / 2
    d = (sum1 + n ** 2 - sum2 - sum3) / 2
    return a, b, c, d


def contingency_table(seg, gt, ignore_seg=[0], ignore_gt=[0], norm=True):
    """Return the contingency table for all regions in matched segmentations."""
    gtr = gt.ravel()
    segr = seg.ravel()
    ij = np.zeros((2, len(gtr)))
    ij[0, :] = segr
    ij[1, :] = gtr
    cont = coo_matrix((np.ones((len(gtr))), ij)).toarray()
    cont[:, ignore_gt] = 0
    cont[ignore_seg, :] = 0
    if norm:
        cont /= float(cont.sum())
    return cont


def simpleG(L, C, f, t, pl, ph, wnconst):
    # L-number of layers
    # C-number of clusters
    # f-min size of cluster
    # t-max size of cluster
    # within community probability is sampled from runif(from=pl,to=ph)
    # wnconst is the levels of noise introduced where probability for an edge in white noise is wnconst/N where N is the number of nodes in each layer; 1/N means that one edge for each node is white noise
    lbl = int(1)
    sz = []
    cls = []
    blg = []
    for i in np.arange(C):
        size = int(np.random.choice(np.arange(f, t + 10, 10), 1))
        sz.append(size)
        cl = np.empty(shape=[L, size, size])
        bl = np.empty(shape=[L, 1, size])
        val = np.random.choice(np.arange(L), 1)
        if val == (L - 1):
            for l in np.arange(L):
                cl[l, :, :] = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(size, np.random.uniform(low=pl, high=ph, size=1))).data)
                bl[l, 0, :] = np.array(lbl).repeat(size)
            lbl = int(lbl + 1)
        if val == 0:
            vv = np.random.choice(np.arange(L), 1)
            for l in np.arange(L):
                if vv == l:
                    cl[l, :, :] = np.array(ig.Graph.get_adjacency(
                        ig.Graph.Erdos_Renyi(size, np.random.uniform(low=pl, high=ph, size=1))).data)
                    bl[l, 0, :] = np.array(lbl).repeat(size)
                    lbl = lbl + 1
                else:
                    cl[l, :, :] = np.zeros((size, size), dtype=int)
                    bl[l, 0, :] = np.arange(lbl, int(size + lbl))
                    lbl = int(lbl + size)
        if val > 0 and val < (L - 1):
            vv = np.random.choice(np.arange(L), val + 1)
            lstar = lbl
            lbl = lbl + 1
            for l in np.arange(L):
                if np.any(vv == l):
                    cl[l, :, :] = np.array(ig.Graph.get_adjacency(
                        ig.Graph.Erdos_Renyi(size, np.random.uniform(low=pl, high=ph, size=1))).data)
                    bl[l, 0, :] = np.array(lstar).repeat(size)

                else:
                    cl[l, :, :] = np.zeros((size, size), dtype=int)
                    bl[l, 0, :] = np.arange(lbl, int(size + lbl))
                    lbl = int(lbl + size)
        cls.append(cl)
        blg.append(bl)
    sz = np.array(sz)
    N = sum(sz)
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.empty(shape=(0, 0))
        for j in np.arange(len(cls)):
            mat = LA.block_diag(mat, cls[j][i, :, :])
        wn = np.array(ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(N, wnconst / N)).data)
        mat = mat + wn
        mat[mat > 1] = 1
        np.fill_diagonal(mat, 0)
        Mat[i, :, :] = mat
    orig = np.empty(shape=(0, 0))
    for i in np.arange(L):
        for j in np.arange(len(blg)):
            orig = np.append(orig, blg[j][i, 0, :])
    orig = np.array(orig, dtype=int)
    return Mat, orig


def uniG(L, C, f, t, pl, ph, wnconst):
    # L-number of layers
    # C-number of clusters
    # f-min size of cluster
    # t-max size of cluster
    # within community probability is sampled from runif(from=pl,to=ph)
    # wnconst is the levels of noise introduced where probability for an edge in white noise is wnconst/N where N is the number of nodes in each layer; 1/N means that one edge for each node is white noise
    lbl = int(1)
    sz = []
    cls = []
    blg = []
    for i in np.arange(C):
        size = int(np.random.choice(np.arange(f, t + 10, 10), 1))
        sz.append(size)
        cl = np.empty(shape=[L, size, size])
        bl = np.empty(shape=[L, 1, size])
        nn = int(np.ceil((2 / 3) * size))
        nn2 = int(size - nn)
        lbl1 = np.array(lbl).repeat(size)
        lbl = int(lbl + 1)
        val = np.random.choice(np.arange(L), 1)
        for l in np.arange(L):
            if val == l:
                cl[l, :, :] = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(size, np.random.uniform(low=pl, high=ph, size=1))).data)
                bl[l, 0, :] = lbl1
            else:
                g1 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn, np.random.uniform(low=pl, high=ph, size=1))).data)
                g2 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn2, np.random.uniform(low=pl, high=ph, size=1))).data)
                nstar = nn
                nstar2 = nn2
                nn = nstar2
                nn2 = nstar
                g = LA.block_diag(g1, g2)
                cl[l, :, :] = g
                bl[l, 0, :] = lbl1

        cls.append(cl)
        blg.append(bl)
    sz = np.array(sz)
    N = sum(sz)
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.empty(shape=(0, 0))
        for j in np.arange(len(cls)):
            mat = LA.block_diag(mat, cls[j][i, :, :])
        wn = np.array(ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(N, wnconst / N)).data)
        mat = mat + wn
        mat[mat > 1] = 1
        np.fill_diagonal(mat, 0)
        Mat[i, :, :] = mat
    orig = np.empty(shape=(0, 0))
    for i in np.arange(L):
        for j in np.arange(len(blg)):
            orig = np.append(orig, blg[j][i, 0, :])
    orig = np.array(orig, dtype=int)
    return Mat, orig


def uni2G(L, C, f, t, pl, ph, wnconst):
    # L-number of layers
    # C-number of clusters
    # f-min size of cluster
    # t-max size of cluster
    # within community probability is sampled from runif(from=pl,to=ph)
    # wnconst is the levels of noise introduced where probability for an edge in white noise is wnconst/N where N is the number of nodes in each layer; 1/N means that one edge for each node is white noise
    lbl = int(1)
    sz = []
    cls = []
    blg = []
    for i in np.arange(C):
        size = int(np.random.choice(np.arange(f, t + 10, 10), 1))
        sz.append(size)
        cl = np.empty(shape=[L, size, size])
        bl = np.empty(shape=[L, 1, size])
        nn = int((1 / 2) * size)
        lbl1 = np.array(lbl).repeat(size)
        lbl = int(lbl + 1)
        val = np.random.choice(np.arange(L), 1)
        for l in np.arange(L):
            if val == l:
                cl[l, :, :] = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(size, np.random.uniform(low=pl, high=ph, size=1))).data)
                bl[l, 0, :] = lbl1
            else:
                g1 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn, np.random.uniform(low=pl, high=ph, size=1))).data)
                g2 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn, np.random.uniform(low=pl, high=ph, size=1))).data)
                g = LA.block_diag(g1, g2)
                cl[l, :, :] = g
                bl[l, 0, :] = lbl1

        cls.append(cl)
        blg.append(bl)
    sz = np.array(sz)
    N = sum(sz)
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.empty(shape=(0, 0))
        for j in np.arange(len(cls)):
            mat = LA.block_diag(mat, cls[j][i, :, :])
        wn = np.array(ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(N, wnconst / N)).data)
        mat = mat + wn
        mat[mat > 1] = 1
        np.fill_diagonal(mat, 0)
        Mat[i, :, :] = mat
    orig = np.empty(shape=(0, 0))
    for i in np.arange(L):
        for j in np.arange(len(blg)):
            orig = np.append(orig, blg[j][i, 0, :])
    orig = np.array(orig, dtype=int)
    return Mat, orig


def divG(L, C, f, t, pl, ph, wnconst):
    # L-number of layers
    # C-number of clusters
    # f-min size of cluster
    # t-max size of cluster
    # within community probability is sampled from runif(from=pl,to=ph)
    # wnconst is the levels of noise introduced where probability for an edge in white noise is wnconst/N where N is the number of nodes in each layer; 1/N means that one edge for each node is white noise
    lbl = int(1)
    sz = []
    cls = []
    blg = []
    for i in np.arange(C):
        size = int(np.random.choice(np.arange(f, t + 10, 10), 1))
        sz.append(size)
        cl = np.empty(shape=[L, size, size])
        bl = np.empty(shape=[L, 1, size])
        nn = int((1 / 2) * size)
        lbl1 = np.append(np.array(lbl).repeat(nn), np.array(lbl + 1).repeat(nn))
        lbl = int(lbl + 2)
        val = np.random.choice(np.arange(L), 1)
        for l in np.arange(L):
            # p=np.random.uniform(low=pl,high=ph,size=(L-1)*2)
            # pp=p
            if l != val:
                g1 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn, np.random.uniform(low=0.3, high=ph, size=1))).data)
                g2 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn, np.random.uniform(low=0.3, high=ph, size=1))).data)
                g = LA.block_diag(g1, g2)
                # pp=np.delete(pp,[0,1])
                cl[l, :, :] = g
                bl[l, 0, :] = lbl1
            if l == val:
                g1 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn, np.random.uniform(low=pl, high=0.2, size=1))).data)
                g2 = np.array(
                    ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(nn, np.random.uniform(low=pl, high=0.2, size=1))).data)
                g = LA.block_diag(g1, g2)
                wn = np.array(ig.Graph.get_adjacency(
                    ig.Graph.Erdos_Renyi(2 * nn, np.random.uniform(low=pl, high=0.2, size=1))).data)
                g = g + wn
                g[g > 1] = 1
                np.fill_diagonal(g, 0)
                cl[l, :, :] = g
                bl[l, 0, :] = lbl1
        cls.append(cl)
        blg.append(bl)
    sz = np.array(sz)
    N = sum(sz)
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.empty(shape=(0, 0))
        for j in np.arange(len(cls)):
            mat = LA.block_diag(mat, cls[j][i, :, :])
        wn = np.array(ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(N, wnconst / N)).data)
        mat = mat + wn
        mat[mat > 1] = 1
        np.fill_diagonal(mat, 0)
        Mat[i, :, :] = mat
    orig = np.empty(shape=(0, 0))
    for i in np.arange(L):
        for j in np.arange(len(blg)):
            orig = np.append(orig, blg[j][i, 0, :])
    orig = np.array(orig, dtype=int)
    return Mat, orig


def complexG(L, C, f, t, pl, ph, wnconst):
    # L-number of layers
    # C-number of clusters
    # f-min size of cluster
    # t-max size of cluster
    # within community probability is sampled from runif(from=pl,to=ph)
    # wnconst is the levels of noise introduced where probability for an edge in white noise is wnconst/N where N is the number of nodes in each layer; 1/N means that one edge for each node is white noise
    lbl = int(1)
    sz = []
    cls = []
    blg = []
    for i in np.arange(C):
        size = int(np.random.choice(np.arange(f, t + 20, 20), 1))
        sz.append(size)
        cl = np.empty(shape=[L, size, size])
        bl = np.empty(shape=[L, 1, size])
        lbl1 = lbl
        lbl2 = lbl + 2
        lbl3 = lbl + 2
        lbl4 = lbl + 3
        lbl = lbl + 4
        val = np.random.choice(np.arange(L), 1)
        for l in np.arange(L):
            if l == val:
                temp = int(size / 4)
                g = np.zeros((size, size))
                v1 = np.arange(temp)
                v2 = np.arange((2 * temp), (3 * temp))
                g[np.ix_(v1, v2)] = np.array(ig.Graph.get_adjacency(
                    ig.Graph.Erdos_Renyi(temp, np.random.uniform(low=0.3, high=0.4, size=1))).data)
                v1 = np.arange((temp), (2 * temp))
                v2 = np.arange((3 * temp), (4 * temp))
                g[np.ix_(v1, v2)] = np.array(ig.Graph.get_adjacency(
                    ig.Graph.Erdos_Renyi(temp, np.random.uniform(low=0.3, high=0.4, size=1))).data)
                g = g + g.T
                cl[l, :, :] = g
                bl[l, 0, :] = np.array([lbl1, lbl2, lbl1, lbl2]).repeat(temp)
            if l != val:
                g1 = np.array(ig.Graph.get_adjacency(
                    ig.Graph.Erdos_Renyi(int(size / 2), np.random.uniform(low=pl, high=ph, size=1))).data)
                g2 = np.array(ig.Graph.get_adjacency(
                    ig.Graph.Erdos_Renyi(int(size / 2), np.random.uniform(low=pl, high=ph, size=1))).data)
                g = LA.block_diag(g1, g2)
                cl[l, :, :] = g
                bl[l, 0, :] = np.array([lbl3, lbl4]).repeat(int(size / 2))
        cls.append(cl)
        blg.append(bl)
    sz = np.array(sz)
    N = sum(sz)
    Mat = np.zeros(shape=[L, N, N])
    for i in np.arange(L):
        mat = np.empty(shape=(0, 0))
        for j in np.arange(len(cls)):
            mat = LA.block_diag(mat, cls[j][i, :, :])
        wn = np.array(ig.Graph.get_adjacency(ig.Graph.Erdos_Renyi(N, wnconst / N)).data)
        mat = mat + wn
        mat[mat > 1] = 1
        np.fill_diagonal(mat, 0)
        Mat[i, :, :] = mat
    orig = np.empty(shape=(0, 0))
    for i in np.arange(L):
        for j in np.arange(len(blg)):
            orig = np.append(orig, blg[j][i, 0, :])
    orig = np.array(orig, dtype=int)
    return Mat, orig


def getpart(x, maxmod, N, L):
    P = [cluster_part(vals=np.arange(N * L))]
    for i in np.arange(N * L, N * L + maxmod):
        v1 = x[i].left
        v2 = x[i].right
        v = np.concatenate(([v1], [v2]))
        w = P[i - N * L].vals
        w = np.setdiff1d(w, v)
        newP = cluster_part(vals=np.concatenate((w, [i])))
        P.append(newP)
    val = P[-1].vals
    part = [cluster_part(vals=x[i].orig) for i in val]
    return part


def dict2part(part, N, L):
    n = N * L
    l = len(part)
    p = np.arange(n)
    for i in np.arange(l):
        ind = part[i].vals
        p[ind] = int(i)
    return p


def prcheck(a, P, N, L):
    # This function check whether the multiplex has disconnected components across all given layers. For very large data, it is recommended to do this separately and feed the separate disconnected components into LART to save computational time and memory.
    g = a.sum(axis=0)
    g1 = ig.Graph.Adjacency(g.tolist())
    g2 = g1.clusters()
    memb = np.array(g2.membership)
    if len(set(memb)) > 1:
        lr = np.arange(L).repeat(N)
        for i in np.arange(len(set(memb))):
            ind = np.random.choice(np.where(memb == (np.unique(memb)[i]))[0], 1)
            P[ind, :] = 0.85 * P[ind, :] + 0.15 / (N * L)
    return P


"""
def get_colors(part):
    cls = ['#808080' for i in range(len(part))]
    vals = np.unique(part).tolist()
    vv = (np.bincount(part) > 1).sum()
    clspool = matplotlib.cm.jet(np.linspace(0, 1, vv))
    clspool = [matplotlib.colors.rgb2hex(i) for i in clspool]
    # clspool=np.random.choice(clspool,len(clspool))
    count = 0
    for i in vals:
        if len(np.where(part == i)[0]) > 1:
            for index in np.array(np.where(part == i)).T:
                cls[int(index)] = clspool[count]
            count = count + 1
    return cls
"""
"""
def plot_compare(orig, part, M, L, yes):
    N = np.shape(M)[1]
    L = np.shape(M)[0]
    nmi = ig.compare_communities(part, orig, method='nmi')
    ari = ig.compare_communities(part, orig, method='ari')
    fm = fm_index(part, orig)
    print 'NMI index: ', np.round(nmi, 4), '; FM index: ', np.round(fm, 4), '; ARI index: ', np.round(ari, 4)
    posM = M.sum(axis=0)
    posM = nx.from_numpy_matrix(posM)
    pos = nx.spring_layout(posM, k=2 / np.sqrt(N))
    plt.figure(1)
    plt.suptitle(
        'True and LART partitions: NMI=' + str(np.round(nmi, 4)) + ', FM=' + str(np.round(fm, 4)) + ', ARI=' + str(
            np.round(ari, 4)))
    orig_cls = get_colors(orig)
    for i in np.arange(L):
        plt.subplot(2, L, i + 1)
        m = M[i, :, :]
        g = nx.from_numpy_matrix(m)
        pp = orig_cls[(i * N):((i + 1) * N)]
        nx.draw(g, pos, node_color=pp, node_size=30, with_labels=yes)
        plt.title('Layer ' + str(i) + ': True Partition')
    part_cls = get_colors(part)
    for i in np.arange(L):
        plt.subplot(2, L, i + 1 + L)
        m = M[i, :, :]
        g = nx.from_numpy_matrix(m)
        pp = part_cls[(i * N):((i + 1) * N)]
        nx.draw(g, pos, node_color=pp, node_size=30, with_labels=yes)
        plt.title('Layer ' + str(i) + ': LART Partition')
    plt.show()
"""

def updateDt(Dt, a, network):
    g = supraA(a, 0, network)
    g1 = ig.Graph.Adjacency(g.tolist())
    g2 = g1.clusters()
    memb = np.array(g2.membership)
    n_comp = np.unique(memb)
    if len(n_comp) > 1:
        for i in np.arange(len(n_comp) - 1):
            for j in np.arange((i + 1), len(n_comp)):
                if i != j:
                    row = np.where(memb == n_comp[i])[0]
                    col = np.where(memb == n_comp[j])[0]
                    Dt[np.ix_(row, col)] = 100
                    Dt[np.ix_(col, row)] = 100
    return Dt


def LART(a, eps=1, t=9, gamma=1, linkage='average', n_jobs1=1, network='full'):
    L = np.shape(a)[0]
    N = np.shape(a)[1]
    A = supraA(a, eps, network)
    D = diagA(A)
    P = np.dot(D, A)
    P = prcheck(a, P, N, L)
    Pt = numpy.linalg.matrix_power(P, t)
    Dt = Dmat(Pt, L, D, n_jobs1=n_jobs1)
    Dt = updateDt(Dt, a, network)  # used only until AgglomerativeClustering function below is not improved
    sA = supraA(a, 0, network)
    model = AgglomerativeClustering(linkage=linkage, compute_full_tree=True, connectivity=sA, affinity='precomputed')
    model.fit(Dt)
    x = model.children_
    x = getorder(x)
    mod = modMLPX(x, a, gamma, network)
    maxmod = mod.max()
    maxmodind = np.argmax(mod)
    part = getpart(x, maxmodind, N, L)
    part = dict2part(part, N, L)
    return Dt, part, mod, maxmod


####RUN LART
if __name__ == '__main__':
    n_jobs1 = 4  # this is the number of cores you want to use for the code to run
    L = 3  # Numbers of layers
    C = 4  # Numbres of possible communities per layer
    f = 40  # min size of community, for complex case this needs to be divisible by 4 for 'complex' case
    to = 100  # max size of community, for complex case this needs to be divisible by 4 for 'complex' case
    pl = 0.15  # min within community probability
    ph = 0.4  # max within community probability
    wn = 1  # white noise levels where probability for edge is wn/N

    synth_graphs = 0

    if synth_graphs:
        if len(sys.argv) > 1:
            if sys.argv[1] == 'simple':
                a, orig = simpleG(L, C, f, to, pl, ph, wn)
            if sys.argv[1] == 'unifying':
                a, orig = uniG(L, C, f, to, pl, ph, wn)
            if sys.argv[1] == 'unifying2':
                a, orig = uni2G(L, C, f, to, pl, ph, wn)
            if sys.argv[1] == 'divisive':
                a, orig = divG(L, C, f, to, pl, ph, wn)
            if sys.argv[1] == 'complex':
                a, orig = complexG(L, C, f, to, pl, ph, wn)
        else:
            a, orig = uniG(L, C, f, to, pl, ph, wn)

    #os.chdir('/home/rob/DATA/air-multi-public-dataset/')
    #input_path = "airlines_remapped.ncol"

    input_path = sys.argv[1]
    a = getAmatrix2(input_path)

    t = 9  # number of steps for random walker to take
    eps = 1  # for binary matrices this will mean adding a self-loop to each node at each layer
    gamma = 1  # any non-negative value recommended 1
    linkage = 'average'  # or complete
    network = 'full'  # or dynamic
    #np.set_printoptions(threshold='nan')
    Dt, part, mod, maxmod = LART(a, eps, t, gamma, linkage, n_jobs1, network)
    merged_parts= partitionsProcessing(part, a)
    
    #Q = multislice_modularity(readGraph(input_path), merged_parts)
    #print("Modularity: ", Q)

    """
    print part[0:N]
    print '\n========================================================================\n'
    print part[N:2*N]
    print '\n========================================================================\n'
    print part[2*N:3*N]
    print '\n========================================================================\n'
    print mod[0:N]
    print '\n========================================================================\n'
    print mod[N:2 * N]
    print '\n========================================================================\n'
    print mod[2 * N:3 * N]
    print '\n========================================================================\n'
    print maxmod
    print '\n========================================================================\n'
    """
    #print Dt,part,mod,maxmod
    #plot_compare(orig, part, a, L, False)
    #plot_compare(part, part, a, L, False)
