import sys
from os.path import basename
import os



lart_coms_file_path = sys.argv[1]
act_ids_file_path = sys.argv[2]
#lay_ids_file_path = sys.argv[3]
output_file = sys.argv[3]
#output_folder = sys.argv[4]
#iteration = sys.argv[5]
#dataset_name = sys.argv[6]


act_ids = {}
lay_ids = {}

#read actor ids into a dictionary
file = open(act_ids_file_path,'r')
for line in file:
    vals = line.split(' ')
    act_ids[int(vals[1])]= vals[0]

#print(lay_ids)

#open the output file for writing
#output_coms_file = open("%s/%s_lart-%s" % (output_folder,dataset_name,iteration),'w')

output_coms_file = open(output_file,'w')

#read form the lart com file and convert to a standard form 
file = open(lart_coms_file_path,'r')
#print(lay_ids)
#print(act_ids)
aid = 1
for line in file:
    output_coms_file.write(str(act_ids[aid]) +","+line)
    aid = aid+1

output_coms_file.close()
