This code implements the clustering method described in:

Xiaowen Dong, Pascal Frossard, Pierre Vandergheynst and Nikolai Nefedov, “Clusteringon Multi-Layer Graphs via Subspace Analysis on Grassmann Manifold,” IEEE Transactionson Signal Processing, vol. 62, no. 4, pp. 905-918, February 2014.

===acknowledgement===
We reuse some code in the SGWT toolbox (http://wiki.epfl.ch/sgwt), originally written by David Hammond, for a function in our implementation.

===usage===
Unzip and put the files in your MATLAB directory. Then, simply run the demo program:
>> demo

===contact===
If you have any questions, please contact: xdong@mit.edu.

Thank you for your interests in our work.