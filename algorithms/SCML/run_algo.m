pkg load statistics

clear;close all;clc

%% reading network
arg_list = argv();
%printf("%s\n", arg_list{1})
run(arg_list{1});
%% read number of communities
run(arg_list{3})

%% run algorithm
lambda = 0.5;
idx = sc_ml(A,K,lambda)

%% obtain actor names
data = fileread(arg_list{2});
actors = strsplit(data);

outfile = fopen(arg_list{4}, "w");

for i=1:numel(idx)
    fprintf(outfile, "%s,%d\n", actors{i}, idx(i));
end
fflush(outfile);
fclose(outfile);
