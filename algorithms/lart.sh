source venv-lart/bin/activate

cd algorithms/LART

python LART.py $1/net.ncol #> /dev/null 2>&1
python lart_coms_convertor.py  ./coms.txt $1/actors_ids.txt  $1/layers_ids.txt $2 #> /dev/null 2>&1
cd ..
cd ..

deactivate
