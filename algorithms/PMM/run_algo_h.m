pkg load statistics

clear;close all;clc

%% reading network
arg_list = argv();
b=load(arg_list{1});
M=max(b(:,1));
N=max(max(b(:,2)),max(b(:,3)));
for n=1:M,
    A{n}=zeros(N,N);
end
for n=1:max(size(b)),
    A{b(n,1)}(b(n,2),b(n,3))=1;
    A{b(n,1)}(b(n,3),b(n,2))=1;
end

Ell=int32(N/2)

%% read number of communities
run(arg_list{3})

[idx] = PMM(A, K, Ell, 5);

%% obtain actor names
data = fileread(arg_list{2});
actors = strsplit(data);

outfile = fopen(arg_list{4}, "w");

for i=1:numel(idx)
    fprintf(outfile, "%s,%d\n", actors{i}, idx(i));
end
fflush(outfile);
fclose(outfile);
