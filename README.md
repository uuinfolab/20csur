# README #

This repository contains the code to replicate the experiments of the article:

Community Detection in Multiplex Networks

by: Matteo Magnani, Obaida Hanteer, Roberto Interdonato, Luca Rossi, and Andrea Tagarelli

currently under submission.   

### Requirements ###

This repository contains algorithms in several languages (C++, python2.7, python3, octave, R), so executing the experiments has the following requirements:

* A recent version of git.
* The *cmake* build system (>=3.15)
* A modern, C++14-ready compiler.
* Octave
* python2.7
* python3
* R
* a terminal to run the .sh scripts (bash)

### Set up ###

1) Install the multinet library from: https://magnanim@bitbucket.org/uuinfolab/uunet.git

2) Install the bash program: timeout

3) For octave, install the io and statistics packages:

```sh
pkg install -forge io
pkg install -forge statistics
```

4) For python, you should create two virtual environments, one called venv (with python3) and one called venv-lart (with python 2.7).

On venv, install:

```sh
pip install sklearn
pip install python-igraph
pip install networkx
pip install seaborn
pip install future
```

On venv-lart, install:

decorator==4.4.2
networkx==2.2
numpy==1.10.2
python-igraph==0.8.2
scikit-learn==0.17
scipy==1.0.0
texttable==1.6.2

### Execute ###

The script: 

```sh
./exec_all.sh 
```

executes all the experiments. The script calls additional .sh files running specific parts (executing the algorithms, plotting the results, etc.). The whole execution can take a few days, you can modify exec_all.sh to selectively run only some parts of the experiments (e.g., not the scalability tests) and deselect some algorithms or datasets.
